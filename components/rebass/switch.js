import styled from 'styled-components'
import { Box, Flex, Text, Switch } from 'rebass'
import { bool, string, func, element, node } from 'prop-types';

const Swtch = styled(Switch)`
  height: 18px;
  width: 36px;
  box-shadow: none;
  background-color: ${props => props.theme.colors.inset};
  &:after {
    height: 12px;
    width: 12px;
    margin: 3px;
    background-color: ${props => props.checked ? props.theme.colors.primary:props.theme.colors.white};
  }
`

Swtch.propTypes = {
  checked: bool
}

export { Swtch }
