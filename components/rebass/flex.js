import { Flex, Box } from 'rebass'
import styled from 'styled-components'

export const FlexBetween = styled(Flex)`
  align-items: center;
  justify-content: space-between;
`

export const FlexAligned = styled(Flex)`
  align-items: center;
`

export const Boxed = styled(Box)`
  padding: 0.5rem;
  background-color: ${props => props.theme.colors.inset};
  border-radius: 3px;
`

export const FlexColumn = styled(Flex)`
  flex-direction: column;
`
