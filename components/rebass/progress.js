import React, { Component } from "react"
import styled from 'styled-components'
import { number, bool, string, func, arrayOf, node } from 'prop-types';
import theme from '../lib/theme'

const Progress = ({labeled,width,progress,aborted,states}) => {
  return <ProgressBar labeled={labeled} width={width} color={aborted ? theme.colors.danger:theme.colors.well}>
    <ProgressBar width={width} progress={progress} color={theme.colors.primary}/>
    {states.map((s,i)=>{
      return <Pt
        width={width}
        first={i==0}
        last={i==(states.length - 1)}
        key={i}
        label={s}
        color={theme.colors.black}
        labeled={labeled}
        position={i*(100/(states.length - 1))}
      />
    })}
  </ProgressBar>
}

Progress.defaultProps = {
  width:4
}

Progress.propTypes = {
  labeled: bool,
  width: number.isRequired,
  progress: number.isRequired,
  aborted: bool,
  states: arrayOf(string).isRequired
}

const ProgressBar = styled.div`
  height: ${props => props.width}px;
  width: ${props => !!props.progress ? (props.progress*100 + "%"):"100%"};
  border-radius: ${props => props.width / 2}px;
  background-color: ${props => props.color};
  transition: all 200ms ease-in-out;
  position: relative;
  overflow: visible;
  margin-bottom: ${props => {
    return props.labeled ? "1rem":"0";
  }}
`

ProgressBar.propTypes = {
  labeled: bool,
  width: number.isRequired,
  color: string.isRequired,
  progress: number,
}

const Pt = styled.div`
  position: absolute;
  height: ${props => props.width}px;
  width: ${props => props.width}px;
  border-radius: ${props => props.width/2}px;
  background-color: ${props => props.color};
  left: ${props => props.position == 100 ? `calc(100% - ${props.width}px)`: props.position+"%"};
  top: 0px;
  bottom: 0px;
  &:after {
    width: 120px;
    text-align: ${props => {
      if (props.first)
        return "left";
      if (props.last)
        return "right";
      return "center";
    }};
    content:'${props => props.label}';
    display:${props => props.labeled ? "initial":"none"};
    position: absolute;
    font-size: 12px;
    color: black;
    top: 0.5rem;
    right: ${props => {
      if (props.first)
        return `-${120 - props.width}px`;
      if (props.last)
        return `0`;
      return `-${60 - props.width / 2}px`;
    }};
  }
`

Pt.propTypes = {
  first: bool,
  last: bool,
  label: string,
  labeled: bool,
  width: number.isRequired,
  color: string.isRequired,
  position: number.isRequired,
}

export { Progress, ProgressBar, Pt }
