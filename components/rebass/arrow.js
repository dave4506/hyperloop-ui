import styled from 'styled-components'
import { oneOf } from 'prop-types';

const Arrw = styled.div`
  display: inline-block;
  width: 0;
  height: 0;
  vertical-align: middle;
  border-right: .3125em solid transparent;
  border-left: .3125em solid transparent;
  border-bottom: .4375em solid;
  transform: ${ props => {
    if(props.direction === 'right')
      return 'rotate(90deg)';
    if(props.direction === 'left')
      return 'rotate(-90deg)';
    if(props.direction === 'down')
      return 'rotate(180deg)';
    return 'rotate(0deg)';
  }}
`

Arrw.propTypes = {
  direction: oneOf(['right','left','down','up']).isRequired
}

export { Arrw }
