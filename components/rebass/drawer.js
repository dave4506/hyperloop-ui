import styled from 'styled-components'
import { Box, Flex } from 'rebass'

export const DeskContainer = styled(Flex)`
  padding: 1rem 2rem;
  height: calc(100vh - 4rem);
  width: 100%;
  flex-direction: column;
`
