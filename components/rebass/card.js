import styled from 'styled-components'
import { Box, Flex, Text, Switch } from 'rebass'
import { Swtch, Title } from './index'
import { bool, string, func, element, node } from 'prop-types';

const CardWrapper = styled(Flex)`
  background-color: ${props => props.theme.colors.background};
  padding: 0.5rem;
  border-radius: 3px;
  ${props => props.expand ? "flex-grow:1;":""};
  flex-direction: column;
`

const CardHeader = styled(Flex)`
  justify-content: space-between;
  align-content: center;
`

const CardSecondHeader = styled(Box)``

const CardBody = styled(Flex)`
  flex-grow: 1;
  position: relative;
  flex-direction: column;
  overflow-x: hidden;
  overflow-y: ${props => props.scrollable ? "auto":"hidden"};
`

const CardFooter = styled(Box)`
`

const Subtitle = styled.h4`
  padding-left: 0.5rem;
  font-weight: 400;
  font-size: 14px;
  margin: 0;
  opacity: 0.5;
`

const Card = ({
  scrollable,
  expand,
  checked,
  children,
  title,
  subtitle,
  headerSideDetails,
  headerDetails,
  onSwitchClick,footerDetails,
  ...props}) => {
  return <CardWrapper {...props} expand={expand}>
    <CardHeader>
      <Flex alignItems={"center"}>
        <Title>{title}</Title>
        <Subtitle>{subtitle}</Subtitle>
      </Flex>
      <Box>
        {(()=>{
          if(headerSideDetails == "switch")
            return <Swtch onClick={onSwitchClick} checked={checked}/>
          else
            return headerSideDetails
        })()}
      </Box>
    </CardHeader>
    <CardSecondHeader>
      {headerDetails}
    </CardSecondHeader>
    <CardBody scrollable={scrollable}>
      {children}
    </CardBody>
    <CardFooter>
      {footerDetails}
    </CardFooter>
  </CardWrapper>
}

Card.propTypes = {
  scrollable: bool,
  expand: bool,
  checked: bool,
  children: node.isRequired,
  title: string,
  subtitle: string,
  headerSideDetails: node,
  headerDetails: node,
  onSwitchClick: func,
  footerDetails: node
}

export { Card }
