import React, { Component } from "react"
import { Box, Flex, Text, Pre as Pree } from 'rebass'
import styled from 'styled-components'

export const Title = styled.h3`
  font-weight: 400;
  font-size: 16px;
  margin: 0;
  color: ${props => props.theme.colors.black};
`

export const Body = styled.p`
  font-weight: 400;
  font-size: 14px;
  margin: 0;
  color: ${props => props.theme.colors.black};
`

export const Caption = styled.p`
  font-size: 12px;
  margin: 0;
  color: ${props => props.theme.colors.black};
`

export const Pre = styled(Pree)`
  font-size: 10px;
  font-family: "SF Mono", "Roboto Mono", Menlo, monospace;
  color: ${props => props.theme.colors.white};
`
