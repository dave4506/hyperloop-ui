import React, { Component } from "react"
import styled from 'styled-components'
import { Button, Flex, Box } from 'rebass'
import { statusColors } from '../lib/utility'
import { number, oneOf, bool } from 'prop-types';

const BtnGroup = styled(Box)`
  padding: 0.5rem;
  background-color: ${props => props.theme.colors.inset};
  border-radius: 3px;
  & > div {
    margin: 0.5rem 0;
  }
  & >:first-child {
    margin-top: 0;
  }
  & >:last-child {
    margin-bottom: 0;
  }
`

const TgglGroup = styled(BtnGroup)`
  padding: 0;
  background-color: transparent;
  border-radius: 0px;
  & >:nth-child(${props => Math.floor(props.active / props.perRow) + 1})
  :nth-child(${props => props.active % props.perRow + 1})
   {
    color: ${props => props.theme.colors.black};
    background-color: ${statusColors("secondary")};
  }
`

TgglGroup.propTypes = {
  active: number.isRequired,
  perRow: number.isRequired
}

const BtnRow = styled(Flex)`
  & > * {
    margin: 0 0.2rem;
    flex-grow: 1;
  }
  & >:first-child {
    margin-left: 0;
  }
  & >:last-child {
    margin-right: 0;
  }
`

const Btn = styled(Button)`
  font-size: 12px;
  font-weight: 700;
  background-color: ${props => statusColors(props.status)};
  border-radius: 3px;
  color: ${props => props.inverse ? props.theme.colors.white:props.theme.colors.black};
  opacity: 1;
  transition: opacity 200ms ease-in-out;
  &:hover {
    opacity: 0.7;
  }
`

Btn.defaultProps = {
  status: "normal"
}

Btn.propTypes = {
  inverse: bool,
  status: oneOf(["danger","primary","secondary","background","normal"]).isRequired
}

export { Btn, BtnRow, TgglGroup, BtnGroup }
