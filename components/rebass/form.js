import React, { Component } from "react"
import styled, { keyframes } from 'styled-components'
import { Box, Arrow, Relative, Input, Flex, Label, Textarea, Select } from 'rebass'
import { statusColors } from '../lib/utility'
import { bool, string, func, element, node, oneOf, number } from 'prop-types';

export const FrmGroup = styled(Flex)`
  flex-wrap: wrap;
  padding: 0.5rem;
  background-color: ${props => props.theme.colors.inset};
  border-radius: 3px;
`

export const Inpt = styled(Input)`
  border-radius: 3px;
  border-width: 0;
  box-shadow: 0;
  font-size: 14px;
  color: ${props => props.theme.colors.black};
  background-color: ${props => props.theme.colors.white};
  padding: 0.5rem;
`

export const TxtArea = styled(Textarea)`
  border-radius: 3px;
  border-width: 0;
  box-shadow: 0;
  font-size: 14px;
  color: ${props => props.theme.colors.black};
  background-color: ${props => props.theme.colors.white};
  padding: 0.5rem;
  resize: none;
`

export const InptLabel = styled(Label)`
  font-size: 14px;
  margin-bottom: 0.5rem;
  color: ${props => props.theme.colors.black};
`

export const InptLabelMinor = styled.span`
  padding-left: 0.5rem;
  opacity: 0.5;
`

const Slct = styled(Select)`
  padding: 0.5rem;
  background-color: ${props => props.theme.colors.white};
  font-size: 14px;
  border-radius: 3px;
  border-width: 0;
  box-shadow: 0;
  position: relative;
`

const ArrowWrapper = styled(Flex)`
  justify-content: center; align-items: center;
  z-index: 999; position: absolute; top: 0; bottom: 0;
  right: 12px;
  pointer-events: none;
`

export const Slctr = ({children,...props}) => {
  return <Relative style={{width:"100%"}} {...props}>
    <ArrowWrapper>
      <Arrow direction={"down"}/>
    </ArrowWrapper>
    <Slct>
      {children}
    </Slct>
  </Relative>
}


export const Wll = styled(Flex)`
  padding: 0.5rem;
  background-color: ${props => props.theme.colors.well};
  border-radius: 3px;
  justify-content: center; align-items: center;
  font-size: 12px;
  opacity: 0.8;
`

const ErrorText = styled.span`
  color: ${statusColors("danger")};
  text-align: center;
`

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

export const Ldr = styled.div`
  border: 4px solid white;
  border-top: 4px solid ${props => props.theme.colors.primary};
  border-radius: 50%;
  width: 20px;
  height: 20px;
  animation: ${spin} 1s ease-in-out infinite;
  margin: auto;
`

const StatusWll = ({status,changes}) => {
  return <Wll>
    {(()=>{
      switch(status) {
        case "loading": return <Box>
          <Ldr/>
        </Box>
        case "error":
          return <ErrorText>
            {"Failed to send updates."}
          </ErrorText>
        case "empty":
          return "Up to date."
        case "changed":
          return `${changes} change(s) made.`
      }
    })()}
  </Wll>
}

StatusWll.propTypes = {
    status: oneOf(["loading","error","empty","changed"]).isRequired,
    changes: number
}

export { StatusWll }
