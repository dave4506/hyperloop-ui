import styled from 'styled-components'
import { Box, Flex, Tabs, Tab } from 'rebass'
import { bool, string, func, element, node } from 'prop-types';

const Tbs = styled(Tabs)`
  background-color: ${props => props.theme.colors.inset};
  border: none;
  margin: 0.5rem 0;
  border-radius: 3px;
  & * {
    margin: 0.2rem;
  }
`

const Tb = styled(Tab)`
  font-weight: ${props => props.current ? 700:400};
  cursor: pointer;
  font-size: 12px;
  padding: 0.75rem 0.5rem;
  margin:0;
  border: none;
  opacity: ${props => props.current ? 1:0.5}
  &:hover {
    color: ${props => props.theme.colors.primary};
  }
`

Tb.propTypes = {
  current: bool
}

export { Tb, Tbs }
