import theme from './theme'

export const firstOrLast = (arr,i) => {
  return i == 0 || i == arr.length - 1;
}

export const statusColors = (status) => {
  const colors = theme.colors
  switch (status) {
    case "danger": return colors.danger
    case "primary": return colors.primary
    case "secondary": return colors.secondary
    case "background": return colors.background
    case "normal":
    default: return colors.white
  }
}

export const convertRemToPixels = (rem = 1) =>  {
    return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

export const roundPlaces = (value) => {
  return Math.round(value * 100) / 100
}

export const isRequired = () => {
  throw new Error('param is required');
};
