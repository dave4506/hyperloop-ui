export default {
  colors: {
    background:"#F8F9FC",
    inset:"#E7EFF3",
    well:"#E1E8ED",
    black:"black",
    white:"white",
    primary:"#74b49b",
    danger:"#F67280",
    secondary:"#C4FFDD"
  }
}
