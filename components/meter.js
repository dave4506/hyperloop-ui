import React, { Component } from "react"
import ReactDOM from "react-dom"
import { number, shape, arrayOf, string} from 'prop-types';
import styled from 'styled-components'
import theme from './lib/theme'

import { Box, Flex } from 'rebass'
import { Boxed, Wll, Card } from './rebass'
import { WllStats } from './stats'

import Konva from 'konva';
import {
  Stage,
  Layer,
  Rect,
  Text,
  Line,
  Circle,
  Image,
  Arc,
  Group} from 'react-konva';


const BackgroundArc = ({theme,width,height}) => {
  return <Group>
    <Arc
      x={width/2}
      y={height*3/4}
      clockwise
      innerRadius={50}
      outerRadius={80}
      angle={180}
      fill={theme.colors.well}
    />
  </Group>
}

const MeterGuage = ({theme,width,height,arcAngle1,arcAngle2}) => {
  return <Group>
    <Arc
      x={width/2}
      y={height*3/4}
      innerRadius={50}
      outerRadius={80}
      clockwise
      angle={360-arcAngle1}
      rotation={180+arcAngle1}
      fill={theme.colors.primary}
    />
  </Group>
}

const MinMax = ({theme,min,max,width,height}) => {
  return <Group>
    <Text
      x={width/2 - 80}
      y={height*3/4+8}
      fill={theme.colors.black}
      align="center"
      text={min+""}
      width="30"
      fontSize={10}
    />
    <Text
      x={width/2 + 50}
      y={height*3/4+8}
      fill={theme.colors.black}
      align="center"
      text={max+""}
      width="30"
      fontSize={10}
    />
  </Group>
}

const CurrentValue = ({theme,width,height,unit,value}) => {
  return <Group>
    <Text
      x={width/2 - 50}
      y={height*1/2+16}
      fill={theme.colors.black}
      align="center"
      text={(value+"").slice(0,5)}
      width="100"
      fontSize={16}
    />
    <Text
      x={width/2 - 50}
      y={height*1/2+34}
      fill={theme.colors.black}
      align="center"
      text={unit}
      width="100"
      fontSize={10}
    />
  </Group>
}

export default class Meter extends Component {
  state = {
    px:0,
    py:0
  }

  getInitialProps({}) {

  }

  convertToDegree = (value) => {
    const {data} = this.props;
    const { min, max } = data
    return (value/max)*180;
  }

  render() {
    const { px, py } = this.state;
    const { width, height, data, stats, ...props } = this.props;
    const { min, max, unit, value} = data
    const { min:minStats, max:maxStats, avg } = stats;
    return <Boxed mt={2}>
      <Stage width={width} height={height}>
        <Layer>
          <BackgroundArc theme={theme} width={width} height={height}/>
          <MeterGuage theme={theme} width={width} height={height} arcAngle1={this.convertToDegree(value)} arcAngle2={this.convertToDegree(value)}/>
          <MinMax theme={theme} min={min} max={max} width={width} height={height}/>
          <CurrentValue theme={theme} width={width} height={height} unit={unit} value={value}/>
        </Layer>
      </Stage>
      <WllStats data={[
        {label:"Min",value:minStats},
        {label:"Avg",value:avg},
        {label:"Max",value:maxStats},
      ]}/>
    </Boxed>
  }
}

Meter.propTypes = {
  width: number.isRequired,
  height: number.isRequired,
  stats: shape({
    min: number.isRequired,
    max: number.isRequired,
    avg: number.isRequired,
  }).isRequired,
  data: shape({
    min: number.isRequired,
    max: number.isRequired,
    value: number.isRequired,
    unit: string.isRequired,
  }).isRequired,
}
