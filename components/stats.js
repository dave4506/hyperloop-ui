import React, { Component } from "react"
import { arrayOf, string, shape, node } from 'prop-types';
import { Box, Flex } from 'rebass'
import styled from 'styled-components'
import { Caption, BtnGroup, BtnRow, Wll } from './rebass'

const Stat = ({stat}) => {
  const {label,value} = stat;
  return <Caption style={{textAlign:"center"}}>{label}:{' '}{value}</Caption>
}

class Stats extends Component {
  state = {

  }

  getInitialProps({}) {

  }

  render() {
    const { data, ...props } = this.props;
    return <BtnGroup {...props}>
      <BtnRow>
        <Stat stat={data[0]}/>
        <Stat stat={data[1]}/>
        <Stat stat={data[2]}/>
      </BtnRow>
    </BtnGroup>
  }
}

Stats.propTypes = {
  data: arrayOf(shape({
    label:string.isRequired,
    value:node.isRequired
  })).isRequired
}

class WllStats extends Component {
  state = {

  }

  getInitialProps({}) {

  }

  render() {
    const { data } = this.props;
    return <Wll style={{marginBottom:0,display:"block"}}>
      <BtnRow>
        <Stat stat={data[0]}/>
        <Stat stat={data[1]}/>
        <Stat stat={data[2]}/>
      </BtnRow>
    </Wll>
  }
}

WllStats.propTypes = {
  data: arrayOf(shape({
    label:string.isRequired,
    value:node.isRequired
  })).isRequired
}

export { Stats as default, WllStats };
