import React, { Component } from "react"
import { Box, Flex } from 'rebass'
import { Boxed, Card, StatusWll, Wll, Slctr, Btn, BtnRow, TgglGroup, BtnGroup, Tbs, Tb, Inpt, FrmGroup, InptLabel, InptLabelMinor, TxtArea } from '../rebass'

export default class Comp extends Component {
  state = {

  }

  getInitialProps({}) {

  }

  onSubmit = () => {

  }

  render() {
    return <Box>
      <form onSubmit={this.onSubmit}>
        <Boxed mb={2}>
          <InptLabel>{"Label"}<InptLabelMinor>{" Minor"}</InptLabelMinor></InptLabel>
          <TgglGroup active={5} perRow={3}>
            <BtnRow>
              <Btn>{"Btn 1"}</Btn>
              <Btn>{"Btn 2"}</Btn>
              <Btn>{"Btn 3"}</Btn>
            </BtnRow>
            <BtnRow>
              <Btn>{"Btn 4"}</Btn>
              <Btn>{"Btn 5"}</Btn>
              <Btn>{"Btn 6"}</Btn>
            </BtnRow>
          </TgglGroup>
        </Boxed>
        <BtnGroup mb={2}>
          <BtnRow>
            <Btn status="primary" inverse>{"Primary Action"}</Btn>
            <Btn status="normal">{"Secondary Action"}</Btn>
          </BtnRow>
        </BtnGroup>
        <BtnGroup mb={2}>
          <BtnRow>
            <Btn status="danger" inverse>{"Abort"}</Btn>
          </BtnRow>
        </BtnGroup>
        <FrmGroup mb={2}>
          <InptLabel>{"Label"}<InptLabelMinor>{" Minor"}</InptLabelMinor></InptLabel>
          <Inpt mb={2} placeholder="placeholder"/>
          <Slctr>
            <option>Hello</option>
            <option>Beep</option>
            <option>Boop</option>
          </Slctr>
        </FrmGroup>
        <FrmGroup mb={2}>
          <InptLabel>{"Label"}<InptLabelMinor>{" Minor"}</InptLabelMinor></InptLabel>
          <TxtArea rows={4} defaultValue="placeholder"/>
        </FrmGroup>
        <BtnGroup style={{marginBottom:0}}>
          <BtnRow>
            <Btn status="primary" inverse>{"Upload Settings"}</Btn>
            <StatusWll
              status={"loading"}
              changes={3}
            />
          </BtnRow>
        </BtnGroup>
      </form>
    </Box>
  }
}
