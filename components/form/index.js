import Test1 from './test1';
import Test2 from './test2';

export default [{
  label:"Tab 1",
  form: Test1
},{
  label:"Tab 2",
  form: Test2
}]
