import React, { Component } from "react"
import { Box, Flex } from 'rebass'
import { Boxed, Card, StatusWll, Wll, Slctr, Btn, BtnRow, TgglGroup, BtnGroup, Tbs, Tb, Inpt, FrmGroup, InptLabel, InptLabelMinor, TxtArea } from '../rebass'

export default class Comp extends Component {
  state = {

  }

  getInitialProps({}) {

  }

  onSubmit = () => {

  }

  render() {
    return <Box>
      <form onSubmit={this.onSubmit}>
        <Boxed mb={2}>
          <InptLabel>{"Label"}<InptLabelMinor>{" Minor"}</InptLabelMinor></InptLabel>
          <TgglGroup active={2} perRow={4}>
            <BtnRow>
              <Btn>{"Btn 1"}</Btn>
              <Btn>{"Btn 2"}</Btn>
              <Btn>{"Btn 3"}</Btn>
              <Btn>{"Btn 4"}</Btn>
            </BtnRow>
          </TgglGroup>
        </Boxed>
        <FrmGroup mb={2}>
          <InptLabel>{"Label"}<InptLabelMinor>{" Minor"}</InptLabelMinor></InptLabel>
          <Inpt mb={2} placeholder="placeholder"/>
          <Inpt mb={2} placeholder="placeholder"/>
          <Slctr>
            <option>Hello</option>
            <option>Beep</option>
            <option>Boop</option>
          </Slctr>
        </FrmGroup>
        <Boxed mb={2}>
          <InptLabel>{"Label"}<InptLabelMinor>{" Minor"}</InptLabelMinor></InptLabel>
          <TgglGroup active={0} perRow={2}>
            <BtnRow>
              <Btn>{"Btn 1"}</Btn>
              <Btn>{"Btn 2"}</Btn>
            </BtnRow>
          </TgglGroup>
        </Boxed>
        <Boxed mb={2}>
          <InptLabel>{"Label"}<InptLabelMinor>{" Minor"}</InptLabelMinor></InptLabel>
          <TgglGroup active={0} perRow={2}>
            <BtnRow>
              <Btn>{"Btn 1"}</Btn>
              <Btn>{"Btn 2"}</Btn>
            </BtnRow>
          </TgglGroup>
        </Boxed>
        <BtnGroup style={{marginBottom:0}}>
          <BtnRow>
            <Btn status="primary" inverse>{"Upload Settings"}</Btn>
            <StatusWll
              status={"loading"}
              changes={3}
            />
          </BtnRow>
        </BtnGroup>
      </form>
    </Box>
  }
}
