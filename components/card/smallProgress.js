import React, { Component } from "react"
import styled from 'styled-components'
import ReactDOM from "react-dom"
import { string, array, bool, arrayOf, oneOf, number } from 'prop-types';

import { Box, Flex, Tooltip } from 'rebass'
import { Card, ProgressBar, Pt, Progress, Arrw, Caption, FlexAligned, FlexBetween, Boxed, Title } from '../rebass'

class Comp extends Component {
  state = {

  }

  getInitialProps({}) {

  }

  render() {
    const { current, next, states, aborted, totalProgress, ...props } = this.props
    return <Card {...props}>
      <Boxed>
        <FlexAligned mb={2}>
          <Title>{current}</Title>
          <Arrw direction={"right"}  style={{margin:"0 0.5rem"}}/>
          <Caption>{next}</Caption>
        </FlexAligned>
        <Progress states={states} aborted={aborted} progress={totalProgress}/>
      </Boxed>
    </Card>
  }
}

Comp.propTypes = {
  current: string.isRequired,
  next: string.isRequired,
  states: arrayOf(string).isRequired,
  aborted: bool,
  totalProgress: number.isRequired
}

export { Comp as default }
