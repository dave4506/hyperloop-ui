import React, { Component } from "react"
import ReactDOM from "react-dom"
import styled from 'styled-components'
import { arrayOf, object, shape, string } from 'prop-types';

import { Box, Flex } from 'rebass'
import { Caption, Card } from '../rebass'

import Stats from '../stats'

const Table = styled.table`
  width:100%;
  border-spacing: 0;
  border-collapse: collapse;
  border-radius: 3px;
  overflow: hidden;
`

const Tr = styled.tr`
  margin: 0 1rem;
  background-color: ${props => props.colored ? props.theme.colors.inset:props.theme.colors.well};
`

const Th = styled.th`
  padding: 0.25rem 0.5rem;
  font-size: 10px;
  font-weight: bold;
  text-align: ${props => !!props.align ? props.align:"center"};
`
const Td = styled.td`
  padding: 0.25rem 0.5rem;
  font-size: 10px;
  text-align: ${props => !!props.align ? props.align:"center"};
`

class Comp extends Component {
  state = {
    width:0,
    height:30,
    px:0,
    py:0
  }

  getInitialProps({}) {

  }

  getWidth = () => {
    const width = ReactDOM.findDOMNode(this.refs.card).getBoundingClientRect().width
    this.setState({width:(width - 32)});
  }

  getTableAlignment = (arr,i) => {
    if (i == 0)
      return "left";
    if (i == arr.length - 1)
      return "right";
    return "center"
  }

  render() {
    const { width, height, px, py } = this.state;
    const { title, data={} , ...props } = this.props;
    const { headers=[], content=[] } = data;
    return <Card
      title={title}
      scrollable
      subtitle={""}
      {...props}
    ref="card">
      <Box>
        <Table style={{marginTop:"0.5rem"}}>
          <tbody>
            <Tr colored>
              {headers.map((h,i)=>{
                return <Th
                  align={this.getTableAlignment(headers,i)}
                  key={i}>
                {h}
                </Th>
              })}
            </Tr>
            {content.map((d,i)=>{
              return <Tr
                colored={i % 2 == 1}
                key={i}
              >
                {headers.map((h,j)=>{
                  return <Td
                    key={j}
                    align={this.getTableAlignment(headers,j)}
                  >
                    {content[i][h]}
                  </Td>
                })}
              </Tr>
            })}
          </tbody>
        </Table>
      </Box>
  </Card>
  }
}

Comp.propTypes = {
  data: shape({
    headers:arrayOf(string).isRequired,
    content:arrayOf(object).isRequired
  }).isRequired,
  title: string.isRequired
}

export { Comp as default }
