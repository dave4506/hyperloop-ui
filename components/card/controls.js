import React, { Component } from "react"
import ReactDOM from "react-dom"
import styled from 'styled-components'

import { Box } from 'rebass'
import { Card, StatusWll, Wll, Slctr, Btn, BtnRow, TgglGroup, BtnGroup, Tbs, Tb, Inpt, FrmGroup, InptLabel, InptLabelMinor, TxtArea } from '../rebass'
import forms from '../form'

const ControlTabs = ({tabs,activeTabIndex,onClick}) => {
  return <Tbs
    >
    {tabs.map((t,i)=>{
      return <Tb
        key={i}
        current={activeTabIndex == i}
        onClick={()=>{onClick(i)}}
      >
        {t}
      </Tb>
    })}
  </Tbs>
}

export default class Controls extends Component {
  state = {
    activeTabIndex: 0
  }

  getWidth = () => {
  }

  getInitialProps({}) {

  }

  getHeaderDetailsComp() {
    const { activeTabIndex } = this.state;
    return <ControlTabs
      tabs={forms.map(f => f.label)}
      activeTabIndex={activeTabIndex}
      onClick={(i) => {
        this.setState({activeTabIndex:i})
      }}
    />
  }

  render() {
    const { activeTabIndex } = this.state;
    const { ...props } = this.props
    const Form = forms[activeTabIndex].form
    return <Card
      title={"Controls"}
      scrollable
      subtitle={"Online"}
      headerDetails={this.getHeaderDetailsComp()}
      style={{flexGrow:1,height:"100%"}}
      {...props}
    >
      <Form/>
    </Card>
  }
}
