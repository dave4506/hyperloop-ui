import React, { Component } from "react"
import styled from 'styled-components'
import ReactDOM from "react-dom"
import theme from '../lib/theme'
import { object, shape, string, number } from 'prop-types';

import { Box, Flex } from 'rebass'
import { Card, Pre, Title, Body, Caption, Boxed, Wll, FlexBetween, FlexAligned } from '../rebass'
import { statusColors, convertRemToPixels, roundPlaces } from '../lib/utility'

export default class Comp extends Component {
  state = {

  }

  getInitialProps({}) {

  }

  getColorStatus = (status) => {
    if(status == "Connected") return {
      bkgrd:statusColors("primary"),
      frgrd:theme.colors.white
    };
    if(status == "Offline") return {
      bkgrd:statusColors("danger"),
      frgrd:theme.colors.white
    };
    if(status == "Uninitiated") return {
      bkgrd:statusColors("background"),
      frgrd:theme.colors.black
    };
    if(status == "Connecting...") return {
      bkgrd:statusColors("secondary"),
      frgrd:theme.colors.black
    };
    return {
     bkgrd:statusColors("white"),
     frgrd:theme.colors.black
    };
  }

  render() {
    const { record, status, rate, ...props } = this.props;
    const { frgrd, bkgrd } = this.getColorStatus(status)
    const { number, location, date } = record
    return <Card {...props} ref="card">
      <Boxed style={{flexGrow:1,backgroundColor:bkgrd,color:frgrd}}>
        <Pre style={{fontSize:"14px"}} pb={1}>{status}</Pre>
        <Pre pb={1}>{"Location: " + location}</Pre>
        <Pre>{rate + "ms " + date + " Test No. " + number}</Pre>
      </Boxed>
    </Card>
  }
}

Comp.propTypes = {
  record: shape({
    number:number.isRequired,
    location:string.isRequired,
    date:string.isRequired
  }).isRequired,
  status: string.isRequired,
  rate: number.isRequired
}
