import React, { Component } from "react"
import Stats from '../stats'
import { Card } from '../rebass'
import { Box, Flex } from 'rebass'
import PropTypes from 'prop-types';

class Comp extends Component {
  state = {

  }

  getInitialProps({}) {

  }

  render() {
    const { data, ...props } = this.props;
    return <Card {...props} ref="card">
      <Stats mt={2} data={Object.keys(data).map(k=>{
          return {
            label:k,
            value:data[k]
          }
        })}/>
    </Card>
  }
}

Comp.propTypes = {
  data: PropTypes.object.isRequired
}

export { Comp as default }
