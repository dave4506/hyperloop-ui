import React, { Component } from "react"
import ReactDOM from "react-dom"
import styled from 'styled-components'
import { string, array, bool, arrayOf, oneOf, number } from 'prop-types';

import { Box, Flex } from 'rebass'
import { Card, ProgressBar, Pt, Progress, Arrw, Caption, FlexAligned, FlexBetween, Boxed, Title } from '../rebass'

class ProgressCard extends Component {
  state = {
    width:0,
    height:30,
    image: null
  }

  pullImage = () => {
    const image = new window.Image();
    image.src = "/static/hyper_head.svg";
    image.onload = () => {
      this.setState({
        image: image
      });
    };
  }

  getInitialProps({}) {

  }

  render() {
    const { width, height, image } = this.state;
    const { current, next, states, aborted, totalProgress, ...props } = this.props
    return <Card {...props} ref="card">
      <Boxed style={{flexGrow:"1"}}>
        <FlexAligned mb={2}>
          <Title>{current}</Title>
          <Arrw direction={"right"}  style={{margin:"0 0.5rem"}}/>
          <Caption>{next}</Caption>
        </FlexAligned>
        <Progress labeled width={6} states={states} aborted={aborted} progress={totalProgress}/>
      </Boxed>
    </Card>
  }
}

ProgressCard.propTypes = {
  current: string.isRequired,
  next: string.isRequired,
  states: arrayOf(string).isRequired,
  aborted: bool,
  totalProgress: number.isRequired
}

export { ProgressCard as default }
