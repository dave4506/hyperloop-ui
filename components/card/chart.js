import React, { Component } from "react"
import styled from 'styled-components'
import ReactDOM from "react-dom"
import theme from '../lib/theme'
import { object, number, arrayOf, shape, string, } from 'prop-types';

import { Box, Flex } from 'rebass'
import Meter from '../meter'
import { Card, Caption, Boxed, Wll } from '../rebass'
import { convertRemToPixels, roundPlaces } from '../lib/utility'

import {
  FlexibleWidthXYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  LineSeries
} from 'react-vis';

class Comp extends Component {
  state = {
    width:0,
    height:120,
    px:6,
    py:16,
    meter:false
  }

  getInitialProps({}) {

  }


  getWidth = () => {
    const width = ReactDOM.findDOMNode(this.refs.card).getBoundingClientRect().width
    this.setState({width:(width - convertRemToPixels(3))});
  }

  componentDidMount() {
    this.getWidth()
    window.addEventListener('resize', this.getWidth);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.getWidth);
  }

  render() {
    const { title, meterData, data, stats, ...props } = this.props;
    const { width, height, px, py, meter } = this.state;
    meterData.value = data[data.length - 1].y;
    return <Card
      title={title}
      checked={meter}
      onSwitchClick={()=>{this.setState({meter:!meter})}}
      headerSideDetails={"switch"}
      {...props} ref="card">
      {(()=>{
        if(!meter)
          return <Boxed mt={2}>
            <FlexibleWidthXYPlot height={height}>
              <VerticalGridLines />
              <HorizontalGridLines />
              <XAxis
                title={"Time (s)"}
                position={"middle"}
                tickFormat={(t,_)=>{
                  return (t/1000)
                }}
                />
              <YAxis/>
              <LineSeries color={theme.colors.primary} data={data} />
            </FlexibleWidthXYPlot>
            <Wll>
              <Caption>{"Current: " + roundPlaces(data[data.length - 1].y)}</Caption>
            </Wll>
          </Boxed>
        else
          return <Meter
            data={meterData}
            stats={stats}
            height={height}
            width={width}
          />
      })()}
    </Card>
  }
}

Comp.propTypes = {
  stats: object.isRequired,
  data: arrayOf(shape({
    x: number.isRequired,
    y: number.isRequired
  })).isRequired,
  meterData: object.isRequired,
  title: string.isRequired
}

export { Comp as default }
