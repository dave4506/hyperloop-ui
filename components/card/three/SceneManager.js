import * as THREE from 'three';
import theme from '../../lib/theme'

export default canvas => {
  const scene = buildScene();
  const screenDimensions = canvas.getBoundingClientRect();

  const renderer = buildRender(screenDimensions);
  const camera = buildCamera(screenDimensions);
  const sceneSubjects = createSceneSubjects(scene);

  function buildScene() {
    var scene = new THREE.Scene();
    scene.background = new THREE.Color(theme.colors.inset);
    return scene;
  }

  function buildRender({ width, height }) {
    const renderer = new THREE.WebGLRenderer({canvas: canvas});
    renderer.setSize(width, height);

    return renderer;
  }

  function buildCamera({ width, height }) {
			var camera = new THREE.PerspectiveCamera(75, width/height, 1, 10000);
      camera.position.z = 1000;
      return camera;
  }

  function createSceneSubjects(scene) {
    const sceneSubjects = [];
    var geometry = new THREE.BoxGeometry(700, 700, 700, 10, 10, 10);
    var material = new THREE.MeshBasicMaterial({color: 0xfffff, wireframe: true});
    var cube = new THREE.Mesh(geometry, material);
    scene.add(cube);
    sceneSubjects.push(cube);
    return sceneSubjects;
  }

  function update() {
    sceneSubjects[0].rotation.x += 0.01;
    sceneSubjects[0].rotation.y += 0.01;

    renderer.render(scene, camera);
  }

  function onWindowResize() {
    const { width, height } = canvas;

    screenDimensions.width = width;
    screenDimensions.height = height;

    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.setSize(width, height);
  }

  return {
    update,
    onWindowResize
  }
}
