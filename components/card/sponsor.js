import React, { Component } from "react"
import styled from 'styled-components'
import ReactDOM from "react-dom"
import PropTypes from 'prop-types';

import { Box, Flex } from 'rebass'
import { Card, Pre, Title, Body, Caption, Boxed, Wll, FlexBetween, FlexAligned } from '../rebass'
import { convertRemToPixels, roundPlaces, statusColors } from '../lib/utility'
import Stats from '../stats'
import Meter from '../meter'

const SponserWrapper = styled(Flex)`
  margin: 0.5rem 0;
  & > * {
    margin: 0 0.2rem;
  }
  & >:first-child {
    margin-left: 0;
  }
  & >:last-child {
    margin-right: 0;
  }
`

export default class Comp extends Component {
  state = {

  }

  getInitialProps({}) {

  }

  render() {
    const { ...props } = this.props;
    return <Card {...props} ref="card">
      <Boxed style={{flexGrow:"1"}}>
        <Caption style={{opacity:0.5}}>{"Thanks to our sponsors."}</Caption>
        <SponserWrapper>
          <Wll width={1/5}>{"Coke"}</Wll>
          <Wll width={2/5}>{"Pepsi"}</Wll>
          <Wll width={1/5}>{"Sprite"}</Wll>
          <Wll width={2/5}>{"Koolaid"}</Wll>
        </SponserWrapper>
        <SponserWrapper>
          <Wll width={2/5}>{"Coke"}</Wll>
          <Wll width={1/5}>{"Pepsi"}</Wll>
          <Wll width={2/5}>{"Sprite"}</Wll>
          <Wll width={1/5}>{"Koolaid"}</Wll>
        </SponserWrapper>
      </Boxed>
    </Card>
  }
}
