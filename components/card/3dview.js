import React, { Component } from "react"
import ReactDOM from "react-dom"
import styled from 'styled-components'
import { shape, number } from 'prop-types';

import { Box, Flex, Text } from 'rebass'
import { Card, Boxed } from '../rebass'
import inject3d from './three'
import { WllStats } from '../stats'

class Comp extends Component {
  state = {
    width:0,
    height:80,
    px:6,
    py:16
  }

  getInitialProps({}) {

  }

  componentDidMount() {
    this.getWidth()
    this.detachListener = inject3d(ReactDOM.findDOMNode(this.refs.three))
  }

  componentWillUnmount() {
    this.detachListener();
  }

  getWidth = () => {
    const width = ReactDOM.findDOMNode(this.refs.card).getBoundingClientRect().width
    this.setState({width:(width - 32)});
  }

  render() {
    const { stats , ...props } = this.props;
    const { width, height, px, py } = this.state;
    const { yaw, pitch, roll } = stats;
    return <Card title="Orientation" {...props} ref="card">
      <Boxed mt={2}>
        <Box ref="three" style={{height:"120px",position:"relative"}}>
        </Box>
        <WllStats data={[
            {label:"Roll",value:roll},
            {label:"Yaw",value:yaw},
            {label:"Rotate",value:pitch},
          ]}/>
      </Boxed>
    </Card>
  }
}

Comp.propTypes = {
  stats:  shape({
    yaw: number.isRequired,
    roll: number.isRequired,
    pitch: number.isRequired
  }).isRequired
}

export { Comp as default }
