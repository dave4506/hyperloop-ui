import React, { Component } from "react"
import ReactDOM from "react-dom"
import styled from 'styled-components'
import { number, object, shape, arrayOf, string, oneOf } from 'prop-types';

import { Box, Flex } from 'rebass'
import { WllStats } from '../stats'
import { Card, Caption, Boxed, Wll } from '../rebass'

const batteryColor = (status) => {
  switch (status) {
    case "active": return {
      background:"#74B49B",
      foreground:"#307672"
    }
    case "defective": return {
      background:"#F8B195",
      foreground:"#C06C84"
    }
    case "unactive": return {
      background:"#434343",
      foreground:"#white"
    }
  }
}

const BatteryWrapper = styled(Flex)`
  height: 30px;
  border-radius: 15px;
  background-color: ${props => batteryColor(props.status).background};
  justify-content: space-between;
  align-items: center;
  padding: 0 5px;
`

const BatteryCircle = styled(Flex)`
  height: 20px;
  width: 20px;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  background-color: ${props => batteryColor(props.status).foreground};
`

const Battery = ({voltage,battery,status}) => {
  return <BatteryWrapper mx={1} status={status}>
    <Caption style={{color:'white',marginLeft:"5px"}}>{(voltage+"").slice(0,4)}{" "}v</Caption>
    <BatteryCircle status={status}>
      <Caption style={{color:'white'}}>{battery}</Caption>
    </BatteryCircle>
  </BatteryWrapper>
}

const BatteryRowWrapper = styled(Boxed)`
  margin-top: 0.5rem;
  & > div {
    margin: 0.5rem 0;
  }
  & >:first-child {
    margin-top: 0;
  }
  & >:last-child {
    margin-bottom: 0;
  }
`

const BatteryRow = styled(Flex)`
  & > * {
    flex-grow: 1;
  }
  & >:first-child {
    margin-left: 0;
  }
  & >:last-child {
    margin-right: 0;
  }
`

class Comp extends Component {
  state = {
  }

  getInitialProps({}) {

  }

  render() {
    const { data:batteries, stats={}, ...props } = this.props;
    const { warning, status, batteries:batteryStats } = stats;
    return <Card title={"Batteries"} {...props} ref="card">
      <BatteryRowWrapper>
        <BatteryRow>
          <Battery {...batteries[0]} battery={1} />
          <Battery {...batteries[1]} battery={2} />
          <Battery {...batteries[2]} battery={3} />
          <Battery {...batteries[3]} battery={4} />
        </BatteryRow>
        <BatteryRow>
          <Battery {...batteries[4]} battery={5} />
          <Battery {...batteries[5]} battery={6} />
          <Battery {...batteries[6]} battery={7} />
          <Battery {...batteries[7]} battery={8} />
        </BatteryRow>
        <BatteryRow>
          <Battery {...batteries[8]} battery={9} />
          <Battery {...batteries[9]} battery={10} />
          <Battery {...batteries[10]} battery={11} />
          <Battery {...batteries[11]} battery={12} />
        </BatteryRow>
        <BatteryRow>
          <Battery {...batteries[12]} battery={13} />
          <Battery {...batteries[13]} battery={14} />
          <Battery {...batteries[14]} battery={15} />
          <Battery {...batteries[15]} battery={16} />
        </BatteryRow>
        <WllStats
          data={[
            {label:"Status",value:status+""},
            {label:"Batteries",value:batteryStats+""},
            {label:"Warnings",value:warning+""},
          ]}
        />
      </BatteryRowWrapper>
    </Card>
  }
}

Comp.propTypes = {
  data: arrayOf(shape({
    voltage: number.isRequired,
    status: oneOf(["unactive","active","defective"]).isRequired
  })).isRequired,
  stats: shape({
    status: string.isRequired,
    batteries: number.isRequired,
    warning: number.isRequired
  })
}

export { Comp as default }
