import React, { Component } from "react"
import styled from 'styled-components'
import { Box, Flex, Tab, Lead, Switch } from 'rebass'
import { bool, func } from 'prop-types'

const NavWrapper = styled(Flex)`
  justify-content: space-between;
  align-items: center;
  height: 4rem;
  background-color: ${props => props.theme.colors.background};
`

const LinkTab = styled(Tab)`
  cursor:pointer;
  display: flex;
  align-items: center;
  color: ${props => props.theme.colors.black};
  &:hover {
    color: ${props => props.theme.colors.black};
  }
`

const SwitchTab = styled(LinkTab)`
  &:hover {
    color: black;
  }
`

const MinorText = styled.span`
  font-weight: 400;
  padding-left: 0.5rem;
  font-size: 0.8rem;
`

const Nav = ({checked,onSwitchClick}) => {
  return <NavWrapper px={4}>
    <Box>
      <LinkTab>
        {"Hyperloop"}
        <MinorText>{"Console"}</MinorText>
      </LinkTab>
    </Box>
    <Box>
      <LinkTab mr={0} onClick={onSwitchClick} checked={checked}>
        <MinorText>{checked ? "Controls":"Public View Console"}</MinorText>
      </LinkTab>
    </Box>
  </NavWrapper>
}

Nav.propTypes = {
  onSwitchClick: func.isRequired,
  checked: bool.isRequired
}

export default Nav
