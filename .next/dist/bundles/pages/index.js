module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/card/3dview.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Comp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__three__ = __webpack_require__("./components/card/three/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__stats__ = __webpack_require__("./components/stats.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/3dview.js";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }











var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      width: 0,
      height: 80,
      px: 6,
      py: 16
    }, _this.getWidth = function () {
      var width = __WEBPACK_IMPORTED_MODULE_1_react_dom___default.a.findDOMNode(_this.refs.card).getBoundingClientRect().width;
      _this.setState({ width: width - 32 });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getWidth();
      this.detachListener = Object(__WEBPACK_IMPORTED_MODULE_6__three__["a" /* default */])(__WEBPACK_IMPORTED_MODULE_1_react_dom___default.a.findDOMNode(this.refs.three));
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.detachListener();
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          stats = _props.stats,
          props = _objectWithoutProperties(_props, ["stats"]);

      var _state = this.state,
          width = _state.width,
          height = _state.height,
          px = _state.px,
          py = _state.py;
      var yaw = stats.yaw,
          pitch = stats.pitch,
          roll = stats.roll;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_5__rebass__["g" /* Card */],
        _extends({ title: "Orientation" }, props, { ref: "card", __source: {
            fileName: _jsxFileName,
            lineNumber: 41
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_5__rebass__["b" /* Boxed */],
          { mt: 2, __source: {
              fileName: _jsxFileName,
              lineNumber: 42
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4_rebass__["Box"], { ref: "three", style: { height: "120px", position: "relative" }, __source: {
              fileName: _jsxFileName,
              lineNumber: 43
            }
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__stats__["a" /* WllStats */], { data: [{ label: "Roll", value: roll }, { label: "Yaw", value: yaw }, { label: "Rotate", value: pitch }], __source: {
              fileName: _jsxFileName,
              lineNumber: 45
            }
          })
        )
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Comp.propTypes = {
  stats: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["shape"])({
    yaw: __WEBPACK_IMPORTED_MODULE_3_prop_types__["number"].isRequired,
    roll: __WEBPACK_IMPORTED_MODULE_3_prop_types__["number"].isRequired,
    pitch: __WEBPACK_IMPORTED_MODULE_3_prop_types__["number"].isRequired
  }).isRequired
};



/***/ }),

/***/ "./components/card/battery.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Comp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__stats__ = __webpack_require__("./components/stats.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__rebass__ = __webpack_require__("./components/rebass/index.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/battery.js";

var _templateObject = _taggedTemplateLiteral(["\n  height: 30px;\n  border-radius: 15px;\n  background-color: ", ";\n  justify-content: space-between;\n  align-items: center;\n  padding: 0 5px;\n"], ["\n  height: 30px;\n  border-radius: 15px;\n  background-color: ", ";\n  justify-content: space-between;\n  align-items: center;\n  padding: 0 5px;\n"]),
    _templateObject2 = _taggedTemplateLiteral(["\n  height: 20px;\n  width: 20px;\n  justify-content: center;\n  align-items: center;\n  border-radius: 10px;\n  background-color: ", ";\n"], ["\n  height: 20px;\n  width: 20px;\n  justify-content: center;\n  align-items: center;\n  border-radius: 10px;\n  background-color: ", ";\n"]),
    _templateObject3 = _taggedTemplateLiteral(["\n  margin-top: 0.5rem;\n  & > div {\n    margin: 0.5rem 0;\n  }\n  & >:first-child {\n    margin-top: 0;\n  }\n  & >:last-child {\n    margin-bottom: 0;\n  }\n"], ["\n  margin-top: 0.5rem;\n  & > div {\n    margin: 0.5rem 0;\n  }\n  & >:first-child {\n    margin-top: 0;\n  }\n  & >:last-child {\n    margin-bottom: 0;\n  }\n"]),
    _templateObject4 = _taggedTemplateLiteral(["\n  & > * {\n    flex-grow: 1;\n  }\n  & >:first-child {\n    margin-left: 0;\n  }\n  & >:last-child {\n    margin-right: 0;\n  }\n"], ["\n  & > * {\n    flex-grow: 1;\n  }\n  & >:first-child {\n    margin-left: 0;\n  }\n  & >:last-child {\n    margin-right: 0;\n  }\n"]);

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }










var batteryColor = function batteryColor(status) {
  switch (status) {
    case "active":
      return {
        background: "#74B49B",
        foreground: "#307672"
      };
    case "defective":
      return {
        background: "#F8B195",
        foreground: "#C06C84"
      };
    case "unactive":
      return {
        background: "#434343",
        foreground: "#white"
      };
  }
};

var BatteryWrapper = __WEBPACK_IMPORTED_MODULE_2_styled_components___default()(__WEBPACK_IMPORTED_MODULE_4_rebass__["Flex"])(_templateObject, function (props) {
  return batteryColor(props.status).background;
});

var BatteryCircle = __WEBPACK_IMPORTED_MODULE_2_styled_components___default()(__WEBPACK_IMPORTED_MODULE_4_rebass__["Flex"])(_templateObject2, function (props) {
  return batteryColor(props.status).foreground;
});

var Battery = function Battery(_ref) {
  var voltage = _ref.voltage,
      battery = _ref.battery,
      status = _ref.status;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    BatteryWrapper,
    { mx: 1, status: status, __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      }
    },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_6__rebass__["f" /* Caption */],
      { style: { color: 'white', marginLeft: "5px" }, __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        }
      },
      (voltage + "").slice(0, 4),
      " ",
      "v"
    ),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      BatteryCircle,
      { status: status, __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        }
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_6__rebass__["f" /* Caption */],
        { style: { color: 'white' }, __source: {
            fileName: _jsxFileName,
            lineNumber: 49
          }
        },
        battery
      )
    )
  );
};

var BatteryRowWrapper = __WEBPACK_IMPORTED_MODULE_2_styled_components___default()(__WEBPACK_IMPORTED_MODULE_6__rebass__["b" /* Boxed */])(_templateObject3);

var BatteryRow = __WEBPACK_IMPORTED_MODULE_2_styled_components___default()(__WEBPACK_IMPORTED_MODULE_4_rebass__["Flex"])(_templateObject4);

var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref2;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref2, [this].concat(args))), _this), _this.state = {}, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref3) {
      _objectDestructuringEmpty(_ref3);
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          batteries = _props.data,
          _props$stats = _props.stats,
          stats = _props$stats === undefined ? {} : _props$stats,
          props = _objectWithoutProperties(_props, ["data", "stats"]);

      var warning = stats.warning,
          status = stats.status,
          batteryStats = stats.batteries;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_6__rebass__["g" /* Card */],
        _extends({ title: "Batteries" }, props, { ref: "card", __source: {
            fileName: _jsxFileName,
            lineNumber: 90
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          BatteryRowWrapper,
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 91
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            BatteryRow,
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 92
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[0], { battery: 1, __source: {
                fileName: _jsxFileName,
                lineNumber: 93
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[1], { battery: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 94
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[2], { battery: 3, __source: {
                fileName: _jsxFileName,
                lineNumber: 95
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[3], { battery: 4, __source: {
                fileName: _jsxFileName,
                lineNumber: 96
              }
            }))
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            BatteryRow,
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 98
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[4], { battery: 5, __source: {
                fileName: _jsxFileName,
                lineNumber: 99
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[5], { battery: 6, __source: {
                fileName: _jsxFileName,
                lineNumber: 100
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[6], { battery: 7, __source: {
                fileName: _jsxFileName,
                lineNumber: 101
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[7], { battery: 8, __source: {
                fileName: _jsxFileName,
                lineNumber: 102
              }
            }))
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            BatteryRow,
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 104
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[8], { battery: 9, __source: {
                fileName: _jsxFileName,
                lineNumber: 105
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[9], { battery: 10, __source: {
                fileName: _jsxFileName,
                lineNumber: 106
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[10], { battery: 11, __source: {
                fileName: _jsxFileName,
                lineNumber: 107
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[11], { battery: 12, __source: {
                fileName: _jsxFileName,
                lineNumber: 108
              }
            }))
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            BatteryRow,
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 110
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[12], { battery: 13, __source: {
                fileName: _jsxFileName,
                lineNumber: 111
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[13], { battery: 14, __source: {
                fileName: _jsxFileName,
                lineNumber: 112
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[14], { battery: 15, __source: {
                fileName: _jsxFileName,
                lineNumber: 113
              }
            })),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Battery, _extends({}, batteries[15], { battery: 16, __source: {
                fileName: _jsxFileName,
                lineNumber: 114
              }
            }))
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__stats__["a" /* WllStats */], {
            data: [{ label: "Status", value: status + "" }, { label: "Batteries", value: batteryStats + "" }, { label: "Warnings", value: warning + "" }],
            __source: {
              fileName: _jsxFileName,
              lineNumber: 116
            }
          })
        )
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Comp.propTypes = {
  data: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["arrayOf"])(Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["shape"])({
    voltage: __WEBPACK_IMPORTED_MODULE_3_prop_types__["number"].isRequired,
    status: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["oneOf"])(["unactive", "active", "defective"]).isRequired
  })).isRequired,
  stats: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["shape"])({
    status: __WEBPACK_IMPORTED_MODULE_3_prop_types__["string"].isRequired,
    batteries: __WEBPACK_IMPORTED_MODULE_3_prop_types__["number"].isRequired,
    warning: __WEBPACK_IMPORTED_MODULE_3_prop_types__["number"].isRequired
  })
};



/***/ }),

/***/ "./components/card/chart.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Comp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lib_theme__ = __webpack_require__("./components/lib/theme.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__meter__ = __webpack_require__("./components/meter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__lib_utility__ = __webpack_require__("./components/lib/utility.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_vis__ = __webpack_require__("react-vis");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_vis___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_react_vis__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/chart.js";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }














var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      width: 0,
      height: 120,
      px: 6,
      py: 16,
      meter: false
    }, _this.getWidth = function () {
      var width = __WEBPACK_IMPORTED_MODULE_2_react_dom___default.a.findDOMNode(_this.refs.card).getBoundingClientRect().width;
      _this.setState({ width: width - Object(__WEBPACK_IMPORTED_MODULE_8__lib_utility__["a" /* convertRemToPixels */])(3) });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getWidth();
      window.addEventListener('resize', this.getWidth);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('resize', this.getWidth);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          title = _props.title,
          meterData = _props.meterData,
          data = _props.data,
          stats = _props.stats,
          props = _objectWithoutProperties(_props, ["title", "meterData", "data", "stats"]);

      var _state = this.state,
          width = _state.width,
          height = _state.height,
          px = _state.px,
          py = _state.py,
          meter = _state.meter;

      meterData.value = data[data.length - 1].y;
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_7__rebass__["g" /* Card */],
        _extends({
          title: title,
          checked: meter,
          onSwitchClick: function onSwitchClick() {
            _this2.setState({ meter: !meter });
          },
          headerSideDetails: "switch"
        }, props, { ref: "card", __source: {
            fileName: _jsxFileName,
            lineNumber: 53
          }
        }),
        function () {
          if (!meter) return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_7__rebass__["b" /* Boxed */],
            { mt: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 61
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_9_react_vis__["FlexibleWidthXYPlot"],
              { height: height, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 62
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_vis__["VerticalGridLines"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 63
                }
              }),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_vis__["HorizontalGridLines"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 64
                }
              }),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_vis__["XAxis"], {
                title: "Time (s)",
                position: "middle",
                tickFormat: function tickFormat(t, _) {
                  return t / 1000;
                },
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 65
                }
              }),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_vis__["YAxis"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 72
                }
              }),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_vis__["LineSeries"], { color: __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.primary, data: data, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 73
                }
              })
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_7__rebass__["z" /* Wll */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 75
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_7__rebass__["f" /* Caption */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 76
                  }
                },
                "Current: " + Object(__WEBPACK_IMPORTED_MODULE_8__lib_utility__["b" /* roundPlaces */])(data[data.length - 1].y)
              )
            )
          );else return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__meter__["a" /* default */], {
            data: meterData,
            stats: stats,
            height: height,
            width: width,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 80
            }
          });
        }()
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Comp.propTypes = {
  stats: __WEBPACK_IMPORTED_MODULE_4_prop_types__["object"].isRequired,
  data: Object(__WEBPACK_IMPORTED_MODULE_4_prop_types__["arrayOf"])(Object(__WEBPACK_IMPORTED_MODULE_4_prop_types__["shape"])({
    x: __WEBPACK_IMPORTED_MODULE_4_prop_types__["number"].isRequired,
    y: __WEBPACK_IMPORTED_MODULE_4_prop_types__["number"].isRequired
  })).isRequired,
  meterData: __WEBPACK_IMPORTED_MODULE_4_prop_types__["object"].isRequired,
  title: __WEBPACK_IMPORTED_MODULE_4_prop_types__["string"].isRequired
};



/***/ }),

/***/ "./components/card/controls.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__form__ = __webpack_require__("./components/form/index.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/controls.js";

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }









var ControlTabs = function ControlTabs(_ref) {
  var tabs = _ref.tabs,
      activeTabIndex = _ref.activeTabIndex,
      _onClick = _ref.onClick;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    __WEBPACK_IMPORTED_MODULE_4__rebass__["v" /* Tbs */],
    {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10
      }
    },
    tabs.map(function (t, i) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_4__rebass__["u" /* Tb */],
        {
          key: i,
          current: activeTabIndex == i,
          onClick: function onClick() {
            _onClick(i);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 13
          }
        },
        t
      );
    })
  );
};

var Controls = function (_Component) {
  _inherits(Controls, _Component);

  function Controls() {
    var _ref2;

    var _temp, _this, _ret;

    _classCallCheck(this, Controls);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = Controls.__proto__ || Object.getPrototypeOf(Controls)).call.apply(_ref2, [this].concat(args))), _this), _this.state = {
      activeTabIndex: 0
    }, _this.getWidth = function () {}, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Controls, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref3) {
      _objectDestructuringEmpty(_ref3);
    }
  }, {
    key: "getHeaderDetailsComp",
    value: function getHeaderDetailsComp() {
      var _this2 = this;

      var activeTabIndex = this.state.activeTabIndex;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(ControlTabs, {
        tabs: __WEBPACK_IMPORTED_MODULE_5__form__["a" /* default */].map(function (f) {
          return f.label;
        }),
        activeTabIndex: activeTabIndex,
        onClick: function onClick(i) {
          _this2.setState({ activeTabIndex: i });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var activeTabIndex = this.state.activeTabIndex;

      var props = _objectWithoutProperties(this.props, []);

      var Form = __WEBPACK_IMPORTED_MODULE_5__form__["a" /* default */][activeTabIndex].form;
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_4__rebass__["g" /* Card */],
        _extends({
          title: "Controls",
          scrollable: true,
          subtitle: "Online",
          headerDetails: this.getHeaderDetailsComp(),
          style: { flexGrow: 1, height: "100%" }
        }, props, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 51
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Form, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 59
          }
        })
      );
    }
  }]);

  return Controls;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (Controls);

/***/ }),

/***/ "./components/card/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__3dview__ = __webpack_require__("./components/card/3dview.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_0__3dview__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__battery__ = __webpack_require__("./components/card/battery.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__battery__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chart__ = __webpack_require__("./components/card/chart.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__chart__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__controls__ = __webpack_require__("./components/card/controls.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_3__controls__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__network__ = __webpack_require__("./components/card/network.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_4__network__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__progress__ = __webpack_require__("./components/card/progress.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_5__progress__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__smallProgress__ = __webpack_require__("./components/card/smallProgress.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_6__smallProgress__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sponsor__ = __webpack_require__("./components/card/sponsor.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_7__sponsor__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__stats__ = __webpack_require__("./components/card/stats.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_8__stats__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__table__ = __webpack_require__("./components/card/table.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_9__table__["a"]; });











/***/ }),

/***/ "./components/card/network.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lib_theme__ = __webpack_require__("./components/lib/theme.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__lib_utility__ = __webpack_require__("./components/lib/utility.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/network.js";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }











var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {}, _this.getColorStatus = function (status) {
      if (status == "Connected") return {
        bkgrd: Object(__WEBPACK_IMPORTED_MODULE_7__lib_utility__["c" /* statusColors */])("primary"),
        frgrd: __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.white
      };
      if (status == "Offline") return {
        bkgrd: Object(__WEBPACK_IMPORTED_MODULE_7__lib_utility__["c" /* statusColors */])("danger"),
        frgrd: __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.white
      };
      if (status == "Uninitiated") return {
        bkgrd: Object(__WEBPACK_IMPORTED_MODULE_7__lib_utility__["c" /* statusColors */])("background"),
        frgrd: __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.black
      };
      if (status == "Connecting...") return {
        bkgrd: Object(__WEBPACK_IMPORTED_MODULE_7__lib_utility__["c" /* statusColors */])("secondary"),
        frgrd: __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.black
      };
      return {
        bkgrd: Object(__WEBPACK_IMPORTED_MODULE_7__lib_utility__["c" /* statusColors */])("white"),
        frgrd: __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.black
      };
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          record = _props.record,
          status = _props.status,
          rate = _props.rate,
          props = _objectWithoutProperties(_props, ["record", "status", "rate"]);

      var _getColorStatus = this.getColorStatus(status),
          frgrd = _getColorStatus.frgrd,
          bkgrd = _getColorStatus.bkgrd;

      var number = record.number,
          location = record.location,
          date = record.date;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_6__rebass__["g" /* Card */],
        _extends({}, props, { ref: "card", __source: {
            fileName: _jsxFileName,
            lineNumber: 47
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_6__rebass__["b" /* Boxed */],
          { style: { flexGrow: 1, backgroundColor: bkgrd, color: frgrd }, __source: {
              fileName: _jsxFileName,
              lineNumber: 48
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_6__rebass__["p" /* Pre */],
            { style: { fontSize: "14px" }, pb: 1, __source: {
                fileName: _jsxFileName,
                lineNumber: 49
              }
            },
            status
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_6__rebass__["p" /* Pre */],
            { pb: 1, __source: {
                fileName: _jsxFileName,
                lineNumber: 50
              }
            },
            "Location: " + location
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_6__rebass__["p" /* Pre */],
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 51
              }
            },
            rate + "ms " + date + " Test No. " + number
          )
        )
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (Comp);


Comp.propTypes = {
  record: Object(__WEBPACK_IMPORTED_MODULE_4_prop_types__["shape"])({
    number: __WEBPACK_IMPORTED_MODULE_4_prop_types__["number"].isRequired,
    location: __WEBPACK_IMPORTED_MODULE_4_prop_types__["string"].isRequired,
    date: __WEBPACK_IMPORTED_MODULE_4_prop_types__["string"].isRequired
  }).isRequired,
  status: __WEBPACK_IMPORTED_MODULE_4_prop_types__["string"].isRequired,
  rate: __WEBPACK_IMPORTED_MODULE_4_prop_types__["number"].isRequired
};

/***/ }),

/***/ "./components/card/progress.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressCard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__rebass__ = __webpack_require__("./components/rebass/index.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/progress.js";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }









var ProgressCard = function (_Component) {
  _inherits(ProgressCard, _Component);

  function ProgressCard() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ProgressCard);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ProgressCard.__proto__ || Object.getPrototypeOf(ProgressCard)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      width: 0,
      height: 30,
      image: null
    }, _this.pullImage = function () {
      var image = new window.Image();
      image.src = "/static/hyper_head.svg";
      image.onload = function () {
        _this.setState({
          image: image
        });
      };
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ProgressCard, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: "render",
    value: function render() {
      var _state = this.state,
          width = _state.width,
          height = _state.height,
          image = _state.image;

      var _props = this.props,
          current = _props.current,
          next = _props.next,
          states = _props.states,
          aborted = _props.aborted,
          totalProgress = _props.totalProgress,
          props = _objectWithoutProperties(_props, ["current", "next", "states", "aborted", "totalProgress"]);

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_5__rebass__["g" /* Card */],
        _extends({}, props, { ref: "card", __source: {
            fileName: _jsxFileName,
            lineNumber: 33
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_5__rebass__["b" /* Boxed */],
          { style: { flexGrow: "1" }, __source: {
              fileName: _jsxFileName,
              lineNumber: 34
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_5__rebass__["j" /* FlexAligned */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 35
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["x" /* Title */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 36
                }
              },
              current
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__rebass__["a" /* Arrw */], { direction: "right", style: { margin: "0 0.5rem" }, __source: {
                fileName: _jsxFileName,
                lineNumber: 37
              }
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["f" /* Caption */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 38
                }
              },
              next
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__rebass__["q" /* Progress */], { labeled: true, width: 6, states: states, aborted: aborted, progress: totalProgress, __source: {
              fileName: _jsxFileName,
              lineNumber: 40
            }
          })
        )
      );
    }
  }]);

  return ProgressCard;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

ProgressCard.propTypes = {
  current: __WEBPACK_IMPORTED_MODULE_3_prop_types__["string"].isRequired,
  next: __WEBPACK_IMPORTED_MODULE_3_prop_types__["string"].isRequired,
  states: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["arrayOf"])(__WEBPACK_IMPORTED_MODULE_3_prop_types__["string"]).isRequired,
  aborted: __WEBPACK_IMPORTED_MODULE_3_prop_types__["bool"],
  totalProgress: __WEBPACK_IMPORTED_MODULE_3_prop_types__["number"].isRequired
};



/***/ }),

/***/ "./components/card/smallProgress.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Comp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__rebass__ = __webpack_require__("./components/rebass/index.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/smallProgress.js";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }









var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {}, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          current = _props.current,
          next = _props.next,
          states = _props.states,
          aborted = _props.aborted,
          totalProgress = _props.totalProgress,
          props = _objectWithoutProperties(_props, ["current", "next", "states", "aborted", "totalProgress"]);

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_5__rebass__["g" /* Card */],
        _extends({}, props, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 20
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_5__rebass__["b" /* Boxed */],
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 21
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_5__rebass__["j" /* FlexAligned */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 22
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["x" /* Title */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 23
                }
              },
              current
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__rebass__["a" /* Arrw */], { direction: "right", style: { margin: "0 0.5rem" }, __source: {
                fileName: _jsxFileName,
                lineNumber: 24
              }
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["f" /* Caption */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 25
                }
              },
              next
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__rebass__["q" /* Progress */], { states: states, aborted: aborted, progress: totalProgress, __source: {
              fileName: _jsxFileName,
              lineNumber: 27
            }
          })
        )
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Comp.propTypes = {
  current: __WEBPACK_IMPORTED_MODULE_3_prop_types__["string"].isRequired,
  next: __WEBPACK_IMPORTED_MODULE_3_prop_types__["string"].isRequired,
  states: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["arrayOf"])(__WEBPACK_IMPORTED_MODULE_3_prop_types__["string"]).isRequired,
  aborted: __WEBPACK_IMPORTED_MODULE_3_prop_types__["bool"],
  totalProgress: __WEBPACK_IMPORTED_MODULE_3_prop_types__["number"].isRequired
};



/***/ }),

/***/ "./components/card/sponsor.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lib_utility__ = __webpack_require__("./components/lib/utility.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__stats__ = __webpack_require__("./components/stats.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__meter__ = __webpack_require__("./components/meter.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/sponsor.js";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(["\n  margin: 0.5rem 0;\n  & > * {\n    margin: 0 0.2rem;\n  }\n  & >:first-child {\n    margin-left: 0;\n  }\n  & >:last-child {\n    margin-right: 0;\n  }\n"], ["\n  margin: 0.5rem 0;\n  & > * {\n    margin: 0 0.2rem;\n  }\n  & >:first-child {\n    margin-left: 0;\n  }\n  & >:last-child {\n    margin-right: 0;\n  }\n"]);

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }












var SponserWrapper = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_4_rebass__["Flex"])(_templateObject);

var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {}, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: "render",
    value: function render() {
      var props = _objectWithoutProperties(this.props, []);

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_5__rebass__["g" /* Card */],
        _extends({}, props, { ref: "card", __source: {
            fileName: _jsxFileName,
            lineNumber: 36
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_5__rebass__["b" /* Boxed */],
          { style: { flexGrow: "1" }, __source: {
              fileName: _jsxFileName,
              lineNumber: 37
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_5__rebass__["f" /* Caption */],
            { style: { opacity: 0.5 }, __source: {
                fileName: _jsxFileName,
                lineNumber: 38
              }
            },
            "Thanks to our sponsors."
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            SponserWrapper,
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 39
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["z" /* Wll */],
              { width: 1 / 5, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 40
                }
              },
              "Coke"
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["z" /* Wll */],
              { width: 2 / 5, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 41
                }
              },
              "Pepsi"
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["z" /* Wll */],
              { width: 1 / 5, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 42
                }
              },
              "Sprite"
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["z" /* Wll */],
              { width: 2 / 5, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                }
              },
              "Koolaid"
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            SponserWrapper,
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 45
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["z" /* Wll */],
              { width: 2 / 5, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 46
                }
              },
              "Coke"
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["z" /* Wll */],
              { width: 1 / 5, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 47
                }
              },
              "Pepsi"
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["z" /* Wll */],
              { width: 2 / 5, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 48
                }
              },
              "Sprite"
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_5__rebass__["z" /* Wll */],
              { width: 1 / 5, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 49
                }
              },
              "Koolaid"
            )
          )
        )
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (Comp);

/***/ }),

/***/ "./components/card/stats.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Comp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stats__ = __webpack_require__("./components/stats.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prop_types__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/components/card/stats.js';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {}, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: 'getInitialProps',
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          data = _props.data,
          props = _objectWithoutProperties(_props, ['data']);

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_2__rebass__["g" /* Card */],
        _extends({}, props, { ref: 'card', __source: {
            fileName: _jsxFileName,
            lineNumber: 18
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__stats__["b" /* default */], { mt: 2, data: Object.keys(data).map(function (k) {
            return {
              label: k,
              value: data[k]
            };
          }), __source: {
            fileName: _jsxFileName,
            lineNumber: 19
          }
        })
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Comp.propTypes = {
  data: __WEBPACK_IMPORTED_MODULE_4_prop_types___default.a.object.isRequired
};



/***/ }),

/***/ "./components/card/table.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Comp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__stats__ = __webpack_require__("./components/stats.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/card/table.js";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral(["\n  width:100%;\n  border-spacing: 0;\n  border-collapse: collapse;\n  border-radius: 3px;\n  overflow: hidden;\n"], ["\n  width:100%;\n  border-spacing: 0;\n  border-collapse: collapse;\n  border-radius: 3px;\n  overflow: hidden;\n"]),
    _templateObject2 = _taggedTemplateLiteral(["\n  margin: 0 1rem;\n  background-color: ", ";\n"], ["\n  margin: 0 1rem;\n  background-color: ", ";\n"]),
    _templateObject3 = _taggedTemplateLiteral(["\n  padding: 0.25rem 0.5rem;\n  font-size: 10px;\n  font-weight: bold;\n  text-align: ", ";\n"], ["\n  padding: 0.25rem 0.5rem;\n  font-size: 10px;\n  font-weight: bold;\n  text-align: ", ";\n"]),
    _templateObject4 = _taggedTemplateLiteral(["\n  padding: 0.25rem 0.5rem;\n  font-size: 10px;\n  text-align: ", ";\n"], ["\n  padding: 0.25rem 0.5rem;\n  font-size: 10px;\n  text-align: ", ";\n"]);

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }











var Table = __WEBPACK_IMPORTED_MODULE_2_styled_components___default.a.table(_templateObject);

var Tr = __WEBPACK_IMPORTED_MODULE_2_styled_components___default.a.tr(_templateObject2, function (props) {
  return props.colored ? props.theme.colors.inset : props.theme.colors.well;
});

var Th = __WEBPACK_IMPORTED_MODULE_2_styled_components___default.a.th(_templateObject3, function (props) {
  return !!props.align ? props.align : "center";
});
var Td = __WEBPACK_IMPORTED_MODULE_2_styled_components___default.a.td(_templateObject4, function (props) {
  return !!props.align ? props.align : "center";
});

var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      width: 0,
      height: 30,
      px: 0,
      py: 0
    }, _this.getWidth = function () {
      var width = __WEBPACK_IMPORTED_MODULE_1_react_dom___default.a.findDOMNode(_this.refs.card).getBoundingClientRect().width;
      _this.setState({ width: width - 32 });
    }, _this.getTableAlignment = function (arr, i) {
      if (i == 0) return "left";
      if (i == arr.length - 1) return "right";
      return "center";
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _state = this.state,
          width = _state.width,
          height = _state.height,
          px = _state.px,
          py = _state.py;

      var _props = this.props,
          title = _props.title,
          _props$data = _props.data,
          data = _props$data === undefined ? {} : _props$data,
          props = _objectWithoutProperties(_props, ["title", "data"]);

      var _data$headers = data.headers,
          headers = _data$headers === undefined ? [] : _data$headers,
          _data$content = data.content,
          content = _data$content === undefined ? [] : _data$content;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_5__rebass__["g" /* Card */],
        _extends({
          title: title,
          scrollable: true,
          subtitle: ""
        }, props, {
          ref: "card", __source: {
            fileName: _jsxFileName,
            lineNumber: 65
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_4_rebass__["Box"],
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 71
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            Table,
            { style: { marginTop: "0.5rem" }, __source: {
                fileName: _jsxFileName,
                lineNumber: 72
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              "tbody",
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 73
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                Tr,
                { colored: true, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 74
                  }
                },
                headers.map(function (h, i) {
                  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                    Th,
                    {
                      align: _this2.getTableAlignment(headers, i),
                      key: i, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 76
                      }
                    },
                    h
                  );
                })
              ),
              content.map(function (d, i) {
                return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  Tr,
                  {
                    colored: i % 2 == 1,
                    key: i,
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 84
                    }
                  },
                  headers.map(function (h, j) {
                    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                      Td,
                      {
                        key: j,
                        align: _this2.getTableAlignment(headers, j),
                        __source: {
                          fileName: _jsxFileName,
                          lineNumber: 89
                        }
                      },
                      content[i][h]
                    );
                  })
                );
              })
            )
          )
        )
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Comp.propTypes = {
  data: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["shape"])({
    headers: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["arrayOf"])(__WEBPACK_IMPORTED_MODULE_3_prop_types__["string"]).isRequired,
    content: Object(__WEBPACK_IMPORTED_MODULE_3_prop_types__["arrayOf"])(__WEBPACK_IMPORTED_MODULE_3_prop_types__["object"]).isRequired
  }).isRequired,
  title: __WEBPACK_IMPORTED_MODULE_3_prop_types__["string"].isRequired
};



/***/ }),

/***/ "./components/card/three/SceneManager.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_three__ = __webpack_require__("three");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_three___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_three__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_theme__ = __webpack_require__("./components/lib/theme.js");



/* harmony default export */ __webpack_exports__["a"] = (function (canvas) {
  var scene = buildScene();
  var screenDimensions = canvas.getBoundingClientRect();

  var renderer = buildRender(screenDimensions);
  var camera = buildCamera(screenDimensions);
  var sceneSubjects = createSceneSubjects(scene);

  function buildScene() {
    var scene = new __WEBPACK_IMPORTED_MODULE_0_three__["Scene"]();
    scene.background = new __WEBPACK_IMPORTED_MODULE_0_three__["Color"](__WEBPACK_IMPORTED_MODULE_1__lib_theme__["a" /* default */].colors.inset);
    return scene;
  }

  function buildRender(_ref) {
    var width = _ref.width,
        height = _ref.height;

    var renderer = new __WEBPACK_IMPORTED_MODULE_0_three__["WebGLRenderer"]({ canvas: canvas });
    renderer.setSize(width, height);

    return renderer;
  }

  function buildCamera(_ref2) {
    var width = _ref2.width,
        height = _ref2.height;

    var camera = new __WEBPACK_IMPORTED_MODULE_0_three__["PerspectiveCamera"](75, width / height, 1, 10000);
    camera.position.z = 1000;
    return camera;
  }

  function createSceneSubjects(scene) {
    var sceneSubjects = [];
    var geometry = new __WEBPACK_IMPORTED_MODULE_0_three__["BoxGeometry"](700, 700, 700, 10, 10, 10);
    var material = new __WEBPACK_IMPORTED_MODULE_0_three__["MeshBasicMaterial"]({ color: 0xfffff, wireframe: true });
    var cube = new __WEBPACK_IMPORTED_MODULE_0_three__["Mesh"](geometry, material);
    scene.add(cube);
    sceneSubjects.push(cube);
    return sceneSubjects;
  }

  function update() {
    sceneSubjects[0].rotation.x += 0.01;
    sceneSubjects[0].rotation.y += 0.01;

    renderer.render(scene, camera);
  }

  function onWindowResize() {
    var width = canvas.width,
        height = canvas.height;


    screenDimensions.width = width;
    screenDimensions.height = height;

    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.setSize(width, height);
  }

  return {
    update: update,
    onWindowResize: onWindowResize
  };
});

/***/ }),

/***/ "./components/card/three/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__SceneManager__ = __webpack_require__("./components/card/three/SceneManager.js");


/* harmony default export */ __webpack_exports__["a"] = (function (containerElement) {
  var canvas = createCanvas(document, containerElement);
  var sceneManager = new __WEBPACK_IMPORTED_MODULE_0__SceneManager__["a" /* default */](canvas);

  bindEventListeners();
  render();

  function createCanvas(document, containerElement) {
    var canvas = document.createElement('canvas');
    containerElement.appendChild(canvas);
    return canvas;
  }

  function bindEventListeners() {
    window.onresize = resizeCanvas;
    resizeCanvas();
  }

  function removeEventListeners() {
    window.removeEventListener('resize', this.resizeCanvas);
  }

  function resizeCanvas() {
    canvas.style.width = '100%';
    canvas.style.height = '100%';
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    sceneManager.onWindowResize();
  }

  function render(time) {
    requestAnimationFrame(render);
    sceneManager.update();
  }

  return removeEventListeners;
});

/***/ }),

/***/ "./components/form/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__test1__ = __webpack_require__("./components/form/test1.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__test2__ = __webpack_require__("./components/form/test2.js");



/* harmony default export */ __webpack_exports__["a"] = ([{
  label: "Tab 1",
  form: __WEBPACK_IMPORTED_MODULE_0__test1__["a" /* default */]
}, {
  label: "Tab 2",
  form: __WEBPACK_IMPORTED_MODULE_1__test2__["a" /* default */]
}]);

/***/ }),

/***/ "./components/form/test1.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rebass__ = __webpack_require__("./components/rebass/index.js");
var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/components/form/test1.js';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {}, _this.onSubmit = function () {}, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: 'getInitialProps',
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: 'render',
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_1_rebass__["Box"],
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 19
          }
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'form',
          { onSubmit: this.onSubmit, __source: {
              fileName: _jsxFileName,
              lineNumber: 20
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["b" /* Boxed */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 21
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["n" /* InptLabel */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 22
                }
              },
              "Label",
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["o" /* InptLabelMinor */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 22
                  }
                },
                " Minor"
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["w" /* TgglGroup */],
              { active: 5, perRow: 3, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 23
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 24
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 25
                    }
                  },
                  "Btn 1"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 26
                    }
                  },
                  "Btn 2"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 27
                    }
                  },
                  "Btn 3"
                )
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 29
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 30
                    }
                  },
                  "Btn 4"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 31
                    }
                  },
                  "Btn 5"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 32
                    }
                  },
                  "Btn 6"
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["d" /* BtnGroup */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 36
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 37
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                { status: 'primary', inverse: true, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 38
                  }
                },
                "Primary Action"
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                { status: 'normal', __source: {
                    fileName: _jsxFileName,
                    lineNumber: 39
                  }
                },
                "Secondary Action"
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["d" /* BtnGroup */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 42
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                { status: 'danger', inverse: true, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 44
                  }
                },
                "Abort"
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["l" /* FrmGroup */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 47
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["n" /* InptLabel */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 48
                }
              },
              "Label",
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["o" /* InptLabelMinor */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 48
                  }
                },
                " Minor"
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__rebass__["m" /* Inpt */], { mb: 2, placeholder: 'placeholder', __source: {
                fileName: _jsxFileName,
                lineNumber: 49
              }
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["r" /* Slctr */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 50
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'option',
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 51
                  }
                },
                'Hello'
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'option',
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 52
                  }
                },
                'Beep'
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'option',
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 53
                  }
                },
                'Boop'
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["l" /* FrmGroup */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 56
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["n" /* InptLabel */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 57
                }
              },
              "Label",
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["o" /* InptLabelMinor */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 57
                  }
                },
                " Minor"
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__rebass__["y" /* TxtArea */], { rows: 4, defaultValue: 'placeholder', __source: {
                fileName: _jsxFileName,
                lineNumber: 58
              }
            })
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["d" /* BtnGroup */],
            { style: { marginBottom: 0 }, __source: {
                fileName: _jsxFileName,
                lineNumber: 60
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 61
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                { status: 'primary', inverse: true, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 62
                  }
                },
                "Upload Settings"
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__rebass__["s" /* StatusWll */], {
                status: "loading",
                changes: 3,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 63
                }
              })
            )
          )
        )
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (Comp);

/***/ }),

/***/ "./components/form/test2.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rebass__ = __webpack_require__("./components/rebass/index.js");
var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/components/form/test2.js';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {}, _this.onSubmit = function () {}, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: 'getInitialProps',
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);
    }
  }, {
    key: 'render',
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_1_rebass__["Box"],
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 19
          }
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'form',
          { onSubmit: this.onSubmit, __source: {
              fileName: _jsxFileName,
              lineNumber: 20
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["b" /* Boxed */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 21
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["n" /* InptLabel */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 22
                }
              },
              "Label",
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["o" /* InptLabelMinor */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 22
                  }
                },
                " Minor"
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["w" /* TgglGroup */],
              { active: 2, perRow: 4, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 23
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 24
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 25
                    }
                  },
                  "Btn 1"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 26
                    }
                  },
                  "Btn 2"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 27
                    }
                  },
                  "Btn 3"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 28
                    }
                  },
                  "Btn 4"
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["l" /* FrmGroup */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 32
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["n" /* InptLabel */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 33
                }
              },
              "Label",
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["o" /* InptLabelMinor */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 33
                  }
                },
                " Minor"
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__rebass__["m" /* Inpt */], { mb: 2, placeholder: 'placeholder', __source: {
                fileName: _jsxFileName,
                lineNumber: 34
              }
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__rebass__["m" /* Inpt */], { mb: 2, placeholder: 'placeholder', __source: {
                fileName: _jsxFileName,
                lineNumber: 35
              }
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["r" /* Slctr */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 36
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'option',
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 37
                  }
                },
                'Hello'
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'option',
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 38
                  }
                },
                'Beep'
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'option',
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 39
                  }
                },
                'Boop'
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["b" /* Boxed */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 42
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["n" /* InptLabel */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 43
                }
              },
              "Label",
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["o" /* InptLabelMinor */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 43
                  }
                },
                " Minor"
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["w" /* TgglGroup */],
              { active: 0, perRow: 2, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 44
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 45
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 46
                    }
                  },
                  "Btn 1"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 47
                    }
                  },
                  "Btn 2"
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["b" /* Boxed */],
            { mb: 2, __source: {
                fileName: _jsxFileName,
                lineNumber: 51
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["n" /* InptLabel */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 52
                }
              },
              "Label",
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["o" /* InptLabelMinor */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 52
                  }
                },
                " Minor"
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["w" /* TgglGroup */],
              { active: 0, perRow: 2, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 53
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
                {
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 54
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 55
                    }
                  },
                  "Btn 1"
                ),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                  __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                  {
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 56
                    }
                  },
                  "Btn 2"
                )
              )
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2__rebass__["d" /* BtnGroup */],
            { style: { marginBottom: 0 }, __source: {
                fileName: _jsxFileName,
                lineNumber: 60
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_2__rebass__["e" /* BtnRow */],
              {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 61
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_2__rebass__["c" /* Btn */],
                { status: 'primary', inverse: true, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 62
                  }
                },
                "Upload Settings"
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__rebass__["s" /* StatusWll */], {
                status: "loading",
                changes: 3,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 63
                }
              })
            )
          )
        )
      );
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (Comp);

/***/ }),

/***/ "./components/lib/theme.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  colors: {
    background: "#F8F9FC",
    inset: "#E7EFF3",
    well: "#E1E8ED",
    black: "black",
    white: "white",
    primary: "#74b49b",
    danger: "#F67280",
    secondary: "#C4FFDD"
  }
});

/***/ }),

/***/ "./components/lib/utility.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firstOrLast */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return statusColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return convertRemToPixels; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return roundPlaces; });
/* unused harmony export isRequired */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__theme__ = __webpack_require__("./components/lib/theme.js");


var firstOrLast = function firstOrLast(arr, i) {
  return i == 0 || i == arr.length - 1;
};

var statusColors = function statusColors(status) {
  var colors = __WEBPACK_IMPORTED_MODULE_0__theme__["a" /* default */].colors;
  switch (status) {
    case "danger":
      return colors.danger;
    case "primary":
      return colors.primary;
    case "secondary":
      return colors.secondary;
    case "background":
      return colors.background;
    case "normal":
    default:
      return colors.white;
  }
};

var convertRemToPixels = function convertRemToPixels() {
  var rem = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
};

var roundPlaces = function roundPlaces(value) {
  return Math.round(value * 100) / 100;
};

var isRequired = function isRequired() {
  throw new Error('param is required');
};

/***/ }),

/***/ "./components/meter.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom__ = __webpack_require__("react-dom");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lib_theme__ = __webpack_require__("./components/lib/theme.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__stats__ = __webpack_require__("./components/stats.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_konva__ = __webpack_require__("konva");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_konva___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_konva__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_konva__ = __webpack_require__("react-konva");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_react_konva___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_react_konva__);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jsxFileName = "/Users/davidsun/Code/hyperloop-ui/components/meter.js";

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }














var BackgroundArc = function BackgroundArc(_ref) {
  var theme = _ref.theme,
      width = _ref.width,
      height = _ref.height;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    __WEBPACK_IMPORTED_MODULE_9_react_konva__["Group"],
    {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      }
    },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_konva__["Arc"], {
      x: width / 2,
      y: height * 3 / 4,
      clockwise: true,
      innerRadius: 50,
      outerRadius: 80,
      angle: 180,
      fill: theme.colors.well,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      }
    })
  );
};

var MeterGuage = function MeterGuage(_ref2) {
  var theme = _ref2.theme,
      width = _ref2.width,
      height = _ref2.height,
      arcAngle1 = _ref2.arcAngle1,
      arcAngle2 = _ref2.arcAngle2;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    __WEBPACK_IMPORTED_MODULE_9_react_konva__["Group"],
    {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      }
    },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_konva__["Arc"], {
      x: width / 2,
      y: height * 3 / 4,
      innerRadius: 50,
      outerRadius: 80,
      clockwise: true,
      angle: 360 - arcAngle1,
      rotation: 180 + arcAngle1,
      fill: theme.colors.primary,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      }
    })
  );
};

var MinMax = function MinMax(_ref3) {
  var theme = _ref3.theme,
      min = _ref3.min,
      max = _ref3.max,
      width = _ref3.width,
      height = _ref3.height;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    __WEBPACK_IMPORTED_MODULE_9_react_konva__["Group"],
    {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      }
    },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_konva__["Text"], {
      x: width / 2 - 80,
      y: height * 3 / 4 + 8,
      fill: theme.colors.black,
      align: "center",
      text: min + "",
      width: "30",
      fontSize: 10,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      }
    }),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_konva__["Text"], {
      x: width / 2 + 50,
      y: height * 3 / 4 + 8,
      fill: theme.colors.black,
      align: "center",
      text: max + "",
      width: "30",
      fontSize: 10,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      }
    })
  );
};

var CurrentValue = function CurrentValue(_ref4) {
  var theme = _ref4.theme,
      width = _ref4.width,
      height = _ref4.height,
      unit = _ref4.unit,
      value = _ref4.value;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    __WEBPACK_IMPORTED_MODULE_9_react_konva__["Group"],
    {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      }
    },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_konva__["Text"], {
      x: width / 2 - 50,
      y: height * 1 / 2 + 16,
      fill: theme.colors.black,
      align: "center",
      text: (value + "").slice(0, 5),
      width: "100",
      fontSize: 16,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78
      }
    }),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9_react_konva__["Text"], {
      x: width / 2 - 50,
      y: height * 1 / 2 + 34,
      fill: theme.colors.black,
      align: "center",
      text: unit,
      width: "100",
      fontSize: 10,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87
      }
    })
  );
};

var Meter = function (_Component) {
  _inherits(Meter, _Component);

  function Meter() {
    var _ref5;

    var _temp, _this, _ret;

    _classCallCheck(this, Meter);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref5 = Meter.__proto__ || Object.getPrototypeOf(Meter)).call.apply(_ref5, [this].concat(args))), _this), _this.state = {
      px: 0,
      py: 0
    }, _this.convertToDegree = function (value) {
      var data = _this.props.data;
      var min = data.min,
          max = data.max;

      return value / max * 180;
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Meter, [{
    key: "getInitialProps",
    value: function getInitialProps(_ref6) {
      _objectDestructuringEmpty(_ref6);
    }
  }, {
    key: "render",
    value: function render() {
      var _state = this.state,
          px = _state.px,
          py = _state.py;

      var _props = this.props,
          width = _props.width,
          height = _props.height,
          data = _props.data,
          stats = _props.stats,
          props = _objectWithoutProperties(_props, ["width", "height", "data", "stats"]);

      var min = data.min,
          max = data.max,
          unit = data.unit,
          value = data.value;
      var minStats = stats.min,
          maxStats = stats.max,
          avg = stats.avg;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_6__rebass__["b" /* Boxed */],
        { mt: 2, __source: {
            fileName: _jsxFileName,
            lineNumber: 120
          }
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_9_react_konva__["Stage"],
          { width: width, height: height, __source: {
              fileName: _jsxFileName,
              lineNumber: 121
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_9_react_konva__["Layer"],
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 122
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(BackgroundArc, { theme: __WEBPACK_IMPORTED_MODULE_4__lib_theme__["a" /* default */], width: width, height: height, __source: {
                fileName: _jsxFileName,
                lineNumber: 123
              }
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(MeterGuage, { theme: __WEBPACK_IMPORTED_MODULE_4__lib_theme__["a" /* default */], width: width, height: height, arcAngle1: this.convertToDegree(value), arcAngle2: this.convertToDegree(value), __source: {
                fileName: _jsxFileName,
                lineNumber: 124
              }
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(MinMax, { theme: __WEBPACK_IMPORTED_MODULE_4__lib_theme__["a" /* default */], min: min, max: max, width: width, height: height, __source: {
                fileName: _jsxFileName,
                lineNumber: 125
              }
            }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(CurrentValue, { theme: __WEBPACK_IMPORTED_MODULE_4__lib_theme__["a" /* default */], width: width, height: height, unit: unit, value: value, __source: {
                fileName: _jsxFileName,
                lineNumber: 126
              }
            })
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__stats__["a" /* WllStats */], { data: [{ label: "Min", value: minStats }, { label: "Avg", value: avg }, { label: "Max", value: maxStats }], __source: {
            fileName: _jsxFileName,
            lineNumber: 129
          }
        })
      );
    }
  }]);

  return Meter;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

/* harmony default export */ __webpack_exports__["a"] = (Meter);


Meter.propTypes = {
  width: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
  height: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
  stats: Object(__WEBPACK_IMPORTED_MODULE_2_prop_types__["shape"])({
    min: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
    max: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
    avg: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired
  }).isRequired,
  data: Object(__WEBPACK_IMPORTED_MODULE_2_prop_types__["shape"])({
    min: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
    max: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
    value: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
    unit: __WEBPACK_IMPORTED_MODULE_2_prop_types__["string"].isRequired
  }).isRequired
};

/***/ }),

/***/ "./components/nav.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/components/nav.js';

var _templateObject = _taggedTemplateLiteral(['\n  justify-content: space-between;\n  align-items: center;\n  height: 4rem;\n  background-color: ', ';\n'], ['\n  justify-content: space-between;\n  align-items: center;\n  height: 4rem;\n  background-color: ', ';\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  cursor:pointer;\n  display: flex;\n  align-items: center;\n  color: ', ';\n  &:hover {\n    color: ', ';\n  }\n'], ['\n  cursor:pointer;\n  display: flex;\n  align-items: center;\n  color: ', ';\n  &:hover {\n    color: ', ';\n  }\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  &:hover {\n    color: black;\n  }\n'], ['\n  &:hover {\n    color: black;\n  }\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  font-weight: 400;\n  padding-left: 0.5rem;\n  font-size: 0.8rem;\n'], ['\n  font-weight: 400;\n  padding-left: 0.5rem;\n  font-size: 0.8rem;\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }






var NavWrapper = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"])(_templateObject, function (props) {
  return props.theme.colors.background;
});

var LinkTab = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Tab"])(_templateObject2, function (props) {
  return props.theme.colors.black;
}, function (props) {
  return props.theme.colors.black;
});

var SwitchTab = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(LinkTab)(_templateObject3);

var MinorText = __WEBPACK_IMPORTED_MODULE_1_styled_components___default.a.span(_templateObject4);

var Nav = function Nav(_ref) {
  var checked = _ref.checked,
      onSwitchClick = _ref.onSwitchClick;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    NavWrapper,
    { px: 4, __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      }
    },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_2_rebass__["Box"],
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        }
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        LinkTab,
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 38
          }
        },
        "Hyperloop",
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          MinorText,
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 40
            }
          },
          "Console"
        )
      )
    ),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_2_rebass__["Box"],
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        }
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        LinkTab,
        { mr: 0, onClick: onSwitchClick, checked: checked, __source: {
            fileName: _jsxFileName,
            lineNumber: 44
          }
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          MinorText,
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 45
            }
          },
          checked ? "Controls" : "Public View Console"
        )
      )
    )
  );
};

Nav.propTypes = {
  onSwitchClick: __WEBPACK_IMPORTED_MODULE_3_prop_types__["func"].isRequired,
  checked: __WEBPACK_IMPORTED_MODULE_3_prop_types__["bool"].isRequired
};

/* harmony default export */ __webpack_exports__["a"] = (Nav);

/***/ }),

/***/ "./components/rebass/arrow.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Arrw; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
var _templateObject = _taggedTemplateLiteral(['\n  display: inline-block;\n  width: 0;\n  height: 0;\n  vertical-align: middle;\n  border-right: .3125em solid transparent;\n  border-left: .3125em solid transparent;\n  border-bottom: .4375em solid;\n  transform: ', '\n'], ['\n  display: inline-block;\n  width: 0;\n  height: 0;\n  vertical-align: middle;\n  border-right: .3125em solid transparent;\n  border-left: .3125em solid transparent;\n  border-bottom: .4375em solid;\n  transform: ', '\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }




var Arrw = __WEBPACK_IMPORTED_MODULE_0_styled_components___default.a.div(_templateObject, function (props) {
  if (props.direction === 'right') return 'rotate(90deg)';
  if (props.direction === 'left') return 'rotate(-90deg)';
  if (props.direction === 'down') return 'rotate(180deg)';
  return 'rotate(0deg)';
});

Arrw.propTypes = {
  direction: Object(__WEBPACK_IMPORTED_MODULE_1_prop_types__["oneOf"])(['right', 'left', 'down', 'up']).isRequired
};



/***/ }),

/***/ "./components/rebass/buttonGroup.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Btn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return BtnRow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return TgglGroup; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return BtnGroup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lib_utility__ = __webpack_require__("./components/lib/utility.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prop_types__);
var _templateObject = _taggedTemplateLiteral(['\n  padding: 0.5rem;\n  background-color: ', ';\n  border-radius: 3px;\n  & > div {\n    margin: 0.5rem 0;\n  }\n  & >:first-child {\n    margin-top: 0;\n  }\n  & >:last-child {\n    margin-bottom: 0;\n  }\n'], ['\n  padding: 0.5rem;\n  background-color: ', ';\n  border-radius: 3px;\n  & > div {\n    margin: 0.5rem 0;\n  }\n  & >:first-child {\n    margin-top: 0;\n  }\n  & >:last-child {\n    margin-bottom: 0;\n  }\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  padding: 0;\n  background-color: transparent;\n  border-radius: 0px;\n  & >:nth-child(', ')\n  :nth-child(', ')\n   {\n    color: ', ';\n    background-color: ', ';\n  }\n'], ['\n  padding: 0;\n  background-color: transparent;\n  border-radius: 0px;\n  & >:nth-child(', ')\n  :nth-child(', ')\n   {\n    color: ', ';\n    background-color: ', ';\n  }\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  & > * {\n    margin: 0 0.2rem;\n    flex-grow: 1;\n  }\n  & >:first-child {\n    margin-left: 0;\n  }\n  & >:last-child {\n    margin-right: 0;\n  }\n'], ['\n  & > * {\n    margin: 0 0.2rem;\n    flex-grow: 1;\n  }\n  & >:first-child {\n    margin-left: 0;\n  }\n  & >:last-child {\n    margin-right: 0;\n  }\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  font-size: 12px;\n  font-weight: 700;\n  background-color: ', ';\n  border-radius: 3px;\n  color: ', ';\n  opacity: 1;\n  transition: opacity 200ms ease-in-out;\n  &:hover {\n    opacity: 0.7;\n  }\n'], ['\n  font-size: 12px;\n  font-weight: 700;\n  background-color: ', ';\n  border-radius: 3px;\n  color: ', ';\n  opacity: 1;\n  transition: opacity 200ms ease-in-out;\n  &:hover {\n    opacity: 0.7;\n  }\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }







var BtnGroup = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Box"])(_templateObject, function (props) {
  return props.theme.colors.inset;
});

var TgglGroup = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(BtnGroup)(_templateObject2, function (props) {
  return Math.floor(props.active / props.perRow) + 1;
}, function (props) {
  return props.active % props.perRow + 1;
}, function (props) {
  return props.theme.colors.black;
}, Object(__WEBPACK_IMPORTED_MODULE_3__lib_utility__["c" /* statusColors */])("secondary"));

TgglGroup.propTypes = {
  active: __WEBPACK_IMPORTED_MODULE_4_prop_types__["number"].isRequired,
  perRow: __WEBPACK_IMPORTED_MODULE_4_prop_types__["number"].isRequired
};

var BtnRow = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"])(_templateObject3);

var Btn = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Button"])(_templateObject4, function (props) {
  return Object(__WEBPACK_IMPORTED_MODULE_3__lib_utility__["c" /* statusColors */])(props.status);
}, function (props) {
  return props.inverse ? props.theme.colors.white : props.theme.colors.black;
});

Btn.defaultProps = {
  status: "normal"
};

Btn.propTypes = {
  inverse: __WEBPACK_IMPORTED_MODULE_4_prop_types__["bool"],
  status: Object(__WEBPACK_IMPORTED_MODULE_4_prop_types__["oneOf"])(["danger", "primary", "secondary", "background", "normal"]).isRequired
};



/***/ }),

/***/ "./components/rebass/card.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Card; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__index__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prop_types__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/components/rebass/card.js';

var _templateObject = _taggedTemplateLiteral(['\n  background-color: ', ';\n  padding: 0.5rem;\n  border-radius: 3px;\n  ', ';\n  flex-direction: column;\n'], ['\n  background-color: ', ';\n  padding: 0.5rem;\n  border-radius: 3px;\n  ', ';\n  flex-direction: column;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  justify-content: space-between;\n  align-content: center;\n'], ['\n  justify-content: space-between;\n  align-content: center;\n']),
    _templateObject3 = _taggedTemplateLiteral([''], ['']),
    _templateObject4 = _taggedTemplateLiteral(['\n  flex-grow: 1;\n  position: relative;\n  flex-direction: column;\n  overflow-x: hidden;\n  overflow-y: ', ';\n'], ['\n  flex-grow: 1;\n  position: relative;\n  flex-direction: column;\n  overflow-x: hidden;\n  overflow-y: ', ';\n']),
    _templateObject5 = _taggedTemplateLiteral(['\n'], ['\n']),
    _templateObject6 = _taggedTemplateLiteral(['\n  padding-left: 0.5rem;\n  font-weight: 400;\n  font-size: 14px;\n  margin: 0;\n  opacity: 0.5;\n'], ['\n  padding-left: 0.5rem;\n  font-weight: 400;\n  font-size: 14px;\n  margin: 0;\n  opacity: 0.5;\n']);



function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }






var CardWrapper = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"])(_templateObject, function (props) {
  return props.theme.colors.background;
}, function (props) {
  return props.expand ? "flex-grow:1;" : "";
});

var CardHeader = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"])(_templateObject2);

var CardSecondHeader = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Box"])(_templateObject3);

var CardBody = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"])(_templateObject4, function (props) {
  return props.scrollable ? "auto" : "hidden";
});

var CardFooter = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Box"])(_templateObject5);

var Subtitle = __WEBPACK_IMPORTED_MODULE_1_styled_components___default.a.h4(_templateObject6);

var Card = function Card(_ref) {
  var scrollable = _ref.scrollable,
      expand = _ref.expand,
      checked = _ref.checked,
      children = _ref.children,
      title = _ref.title,
      subtitle = _ref.subtitle,
      headerSideDetails = _ref.headerSideDetails,
      headerDetails = _ref.headerDetails,
      onSwitchClick = _ref.onSwitchClick,
      footerDetails = _ref.footerDetails,
      props = _objectWithoutProperties(_ref, ['scrollable', 'expand', 'checked', 'children', 'title', 'subtitle', 'headerSideDetails', 'headerDetails', 'onSwitchClick', 'footerDetails']);

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    CardWrapper,
    _extends({}, props, { expand: expand, __source: {
        fileName: _jsxFileName,
        lineNumber: 51
      }
    }),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      CardHeader,
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        }
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"],
        { alignItems: "center", __source: {
            fileName: _jsxFileName,
            lineNumber: 53
          }
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_3__index__["x" /* Title */],
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 54
            }
          },
          title
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          Subtitle,
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 55
            }
          },
          subtitle
        )
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_2_rebass__["Box"],
        {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 57
          }
        },
        function () {
          if (headerSideDetails == "switch") return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__index__["t" /* Swtch */], { onClick: onSwitchClick, checked: checked, __source: {
              fileName: _jsxFileName,
              lineNumber: 60
            }
          });else return headerSideDetails;
        }()
      )
    ),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      CardSecondHeader,
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        }
      },
      headerDetails
    ),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      CardBody,
      { scrollable: scrollable, __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        }
      },
      children
    ),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      CardFooter,
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        }
      },
      footerDetails
    )
  );
};

Card.propTypes = {
  scrollable: __WEBPACK_IMPORTED_MODULE_4_prop_types__["bool"],
  expand: __WEBPACK_IMPORTED_MODULE_4_prop_types__["bool"],
  checked: __WEBPACK_IMPORTED_MODULE_4_prop_types__["bool"],
  children: __WEBPACK_IMPORTED_MODULE_4_prop_types__["node"].isRequired,
  title: __WEBPACK_IMPORTED_MODULE_4_prop_types__["string"],
  subtitle: __WEBPACK_IMPORTED_MODULE_4_prop_types__["string"],
  headerSideDetails: __WEBPACK_IMPORTED_MODULE_4_prop_types__["node"],
  headerDetails: __WEBPACK_IMPORTED_MODULE_4_prop_types__["node"],
  onSwitchClick: __WEBPACK_IMPORTED_MODULE_4_prop_types__["func"],
  footerDetails: __WEBPACK_IMPORTED_MODULE_4_prop_types__["node"]
};



/***/ }),

/***/ "./components/rebass/container.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Container; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_grid_styled__ = __webpack_require__("grid-styled");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_grid_styled___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_grid_styled__);
var _templateObject = _taggedTemplateLiteral(['\n'], ['\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }




var Container = __WEBPACK_IMPORTED_MODULE_0_styled_components___default()(__WEBPACK_IMPORTED_MODULE_1_grid_styled__["Box"])(_templateObject);

/***/ }),

/***/ "./components/rebass/drawer.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeskContainer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rebass__);
var _templateObject = _taggedTemplateLiteral(['\n  padding: 1rem 2rem;\n  height: calc(100vh - 4rem);\n  width: 100%;\n  flex-direction: column;\n'], ['\n  padding: 1rem 2rem;\n  height: calc(100vh - 4rem);\n  width: 100%;\n  flex-direction: column;\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }




var DeskContainer = __WEBPACK_IMPORTED_MODULE_0_styled_components___default()(__WEBPACK_IMPORTED_MODULE_1_rebass__["Flex"])(_templateObject);

/***/ }),

/***/ "./components/rebass/flex.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export FlexBetween */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FlexAligned; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Boxed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return FlexColumn; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
var _templateObject = _taggedTemplateLiteral(['\n  align-items: center;\n  justify-content: space-between;\n'], ['\n  align-items: center;\n  justify-content: space-between;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  align-items: center;\n'], ['\n  align-items: center;\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  padding: 0.5rem;\n  background-color: ', ';\n  border-radius: 3px;\n'], ['\n  padding: 0.5rem;\n  background-color: ', ';\n  border-radius: 3px;\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  flex-direction: column;\n'], ['\n  flex-direction: column;\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }




var FlexBetween = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_0_rebass__["Flex"])(_templateObject);

var FlexAligned = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_0_rebass__["Flex"])(_templateObject2);

var Boxed = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_0_rebass__["Box"])(_templateObject3, function (props) {
  return props.theme.colors.inset;
});

var FlexColumn = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_0_rebass__["Flex"])(_templateObject4);

/***/ }),

/***/ "./components/rebass/form.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FrmGroup; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Inpt; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return TxtArea; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return InptLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return InptLabelMinor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return Slctr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return Wll; });
/* unused harmony export Ldr */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return StatusWll; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lib_utility__ = __webpack_require__("./components/lib/utility.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_prop_types__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/components/rebass/form.js';

var _templateObject = _taggedTemplateLiteral(['\n  flex-wrap: wrap;\n  padding: 0.5rem;\n  background-color: ', ';\n  border-radius: 3px;\n'], ['\n  flex-wrap: wrap;\n  padding: 0.5rem;\n  background-color: ', ';\n  border-radius: 3px;\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  border-radius: 3px;\n  border-width: 0;\n  box-shadow: 0;\n  font-size: 14px;\n  color: ', ';\n  background-color: ', ';\n  padding: 0.5rem;\n'], ['\n  border-radius: 3px;\n  border-width: 0;\n  box-shadow: 0;\n  font-size: 14px;\n  color: ', ';\n  background-color: ', ';\n  padding: 0.5rem;\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  border-radius: 3px;\n  border-width: 0;\n  box-shadow: 0;\n  font-size: 14px;\n  color: ', ';\n  background-color: ', ';\n  padding: 0.5rem;\n  resize: none;\n'], ['\n  border-radius: 3px;\n  border-width: 0;\n  box-shadow: 0;\n  font-size: 14px;\n  color: ', ';\n  background-color: ', ';\n  padding: 0.5rem;\n  resize: none;\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  font-size: 14px;\n  margin-bottom: 0.5rem;\n  color: ', ';\n'], ['\n  font-size: 14px;\n  margin-bottom: 0.5rem;\n  color: ', ';\n']),
    _templateObject5 = _taggedTemplateLiteral(['\n  padding-left: 0.5rem;\n  opacity: 0.5;\n'], ['\n  padding-left: 0.5rem;\n  opacity: 0.5;\n']),
    _templateObject6 = _taggedTemplateLiteral(['\n  padding: 0.5rem;\n  background-color: ', ';\n  font-size: 14px;\n  border-radius: 3px;\n  border-width: 0;\n  box-shadow: 0;\n  position: relative;\n'], ['\n  padding: 0.5rem;\n  background-color: ', ';\n  font-size: 14px;\n  border-radius: 3px;\n  border-width: 0;\n  box-shadow: 0;\n  position: relative;\n']),
    _templateObject7 = _taggedTemplateLiteral(['\n  justify-content: center; align-items: center;\n  z-index: 999; position: absolute; top: 0; bottom: 0;\n  right: 12px;\n  pointer-events: none;\n'], ['\n  justify-content: center; align-items: center;\n  z-index: 999; position: absolute; top: 0; bottom: 0;\n  right: 12px;\n  pointer-events: none;\n']),
    _templateObject8 = _taggedTemplateLiteral(['\n  padding: 0.5rem;\n  background-color: ', ';\n  border-radius: 3px;\n  justify-content: center; align-items: center;\n  font-size: 12px;\n  opacity: 0.8;\n'], ['\n  padding: 0.5rem;\n  background-color: ', ';\n  border-radius: 3px;\n  justify-content: center; align-items: center;\n  font-size: 12px;\n  opacity: 0.8;\n']),
    _templateObject9 = _taggedTemplateLiteral(['\n  color: ', ';\n  text-align: center;\n'], ['\n  color: ', ';\n  text-align: center;\n']),
    _templateObject10 = _taggedTemplateLiteral(['\n  0% { transform: rotate(0deg); }\n  100% { transform: rotate(360deg); }\n'], ['\n  0% { transform: rotate(0deg); }\n  100% { transform: rotate(360deg); }\n']),
    _templateObject11 = _taggedTemplateLiteral(['\n  border: 4px solid white;\n  border-top: 4px solid ', ';\n  border-radius: 50%;\n  width: 20px;\n  height: 20px;\n  animation: ', ' 1s ease-in-out infinite;\n  margin: auto;\n'], ['\n  border: 4px solid white;\n  border-top: 4px solid ', ';\n  border-radius: 50%;\n  width: 20px;\n  height: 20px;\n  animation: ', ' 1s ease-in-out infinite;\n  margin: auto;\n']);

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }







var FrmGroup = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"])(_templateObject, function (props) {
  return props.theme.colors.inset;
});

var Inpt = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Input"])(_templateObject2, function (props) {
  return props.theme.colors.black;
}, function (props) {
  return props.theme.colors.white;
});

var TxtArea = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Textarea"])(_templateObject3, function (props) {
  return props.theme.colors.black;
}, function (props) {
  return props.theme.colors.white;
});

var InptLabel = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Label"])(_templateObject4, function (props) {
  return props.theme.colors.black;
});

var InptLabelMinor = __WEBPACK_IMPORTED_MODULE_1_styled_components___default.a.span(_templateObject5);

var Slct = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Select"])(_templateObject6, function (props) {
  return props.theme.colors.white;
});

var ArrowWrapper = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"])(_templateObject7);

var Slctr = function Slctr(_ref) {
  var children = _ref.children,
      props = _objectWithoutProperties(_ref, ['children']);

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    __WEBPACK_IMPORTED_MODULE_2_rebass__["Relative"],
    _extends({ style: { width: "100%" } }, props, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      }
    }),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      ArrowWrapper,
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        }
      },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_rebass__["Arrow"], { direction: "down", __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        }
      })
    ),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      Slct,
      {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        }
      },
      children
    )
  );
};


var Wll = __WEBPACK_IMPORTED_MODULE_1_styled_components___default()(__WEBPACK_IMPORTED_MODULE_2_rebass__["Flex"])(_templateObject8, function (props) {
  return props.theme.colors.well;
});

var ErrorText = __WEBPACK_IMPORTED_MODULE_1_styled_components___default.a.span(_templateObject9, Object(__WEBPACK_IMPORTED_MODULE_3__lib_utility__["c" /* statusColors */])("danger"));

var spin = Object(__WEBPACK_IMPORTED_MODULE_1_styled_components__["keyframes"])(_templateObject10);

var Ldr = __WEBPACK_IMPORTED_MODULE_1_styled_components___default.a.div(_templateObject11, function (props) {
  return props.theme.colors.primary;
}, spin);

var StatusWll = function StatusWll(_ref2) {
  var status = _ref2.status,
      changes = _ref2.changes;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    Wll,
    {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      }
    },
    function () {
      switch (status) {
        case "loading":
          return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_2_rebass__["Box"],
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 108
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Ldr, {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 109
              }
            })
          );
        case "error":
          return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            ErrorText,
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 112
              }
            },
            "Failed to send updates."
          );
        case "empty":
          return "Up to date.";
        case "changed":
          return changes + ' change(s) made.';
      }
    }()
  );
};

StatusWll.propTypes = {
  status: Object(__WEBPACK_IMPORTED_MODULE_4_prop_types__["oneOf"])(["loading", "error", "empty", "changed"]).isRequired,
  changes: __WEBPACK_IMPORTED_MODULE_4_prop_types__["number"]
};



/***/ }),

/***/ "./components/rebass/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__buttonGroup__ = __webpack_require__("./components/rebass/buttonGroup.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__buttonGroup__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__buttonGroup__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_0__buttonGroup__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "w", function() { return __WEBPACK_IMPORTED_MODULE_0__buttonGroup__["d"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__tabs__ = __webpack_require__("./components/rebass/tabs.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "u", function() { return __WEBPACK_IMPORTED_MODULE_1__tabs__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "v", function() { return __WEBPACK_IMPORTED_MODULE_1__tabs__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__switch__ = __webpack_require__("./components/rebass/switch.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "t", function() { return __WEBPACK_IMPORTED_MODULE_2__switch__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__form__ = __webpack_require__("./components/rebass/form.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "l", function() { return __WEBPACK_IMPORTED_MODULE_3__form__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "m", function() { return __WEBPACK_IMPORTED_MODULE_3__form__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "n", function() { return __WEBPACK_IMPORTED_MODULE_3__form__["c"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "o", function() { return __WEBPACK_IMPORTED_MODULE_3__form__["d"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "r", function() { return __WEBPACK_IMPORTED_MODULE_3__form__["e"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "s", function() { return __WEBPACK_IMPORTED_MODULE_3__form__["f"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "y", function() { return __WEBPACK_IMPORTED_MODULE_3__form__["g"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "z", function() { return __WEBPACK_IMPORTED_MODULE_3__form__["h"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__text__ = __webpack_require__("./components/rebass/text.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_4__text__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "p", function() { return __WEBPACK_IMPORTED_MODULE_4__text__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "x", function() { return __WEBPACK_IMPORTED_MODULE_4__text__["c"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__flex__ = __webpack_require__("./components/rebass/flex.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_5__flex__["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "j", function() { return __WEBPACK_IMPORTED_MODULE_5__flex__["b"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "k", function() { return __WEBPACK_IMPORTED_MODULE_5__flex__["c"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__arrow__ = __webpack_require__("./components/rebass/arrow.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_6__arrow__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__progress__ = __webpack_require__("./components/rebass/progress.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "q", function() { return __WEBPACK_IMPORTED_MODULE_7__progress__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__drawer__ = __webpack_require__("./components/rebass/drawer.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "i", function() { return __WEBPACK_IMPORTED_MODULE_8__drawer__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__container__ = __webpack_require__("./components/rebass/container.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "h", function() { return __WEBPACK_IMPORTED_MODULE_9__container__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__card__ = __webpack_require__("./components/rebass/card.js");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "g", function() { return __WEBPACK_IMPORTED_MODULE_10__card__["a"]; });












/***/ }),

/***/ "./components/rebass/progress.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Progress; });
/* unused harmony export ProgressBar */
/* unused harmony export Pt */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lib_theme__ = __webpack_require__("./components/lib/theme.js");
var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/components/rebass/progress.js';

var _templateObject = _taggedTemplateLiteral(['\n  height: ', 'px;\n  width: ', ';\n  border-radius: ', 'px;\n  background-color: ', ';\n  transition: all 200ms ease-in-out;\n  position: relative;\n  overflow: visible;\n  margin-bottom: ', '\n'], ['\n  height: ', 'px;\n  width: ', ';\n  border-radius: ', 'px;\n  background-color: ', ';\n  transition: all 200ms ease-in-out;\n  position: relative;\n  overflow: visible;\n  margin-bottom: ', '\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  position: absolute;\n  height: ', 'px;\n  width: ', 'px;\n  border-radius: ', 'px;\n  background-color: ', ';\n  left: ', ';\n  top: 0px;\n  bottom: 0px;\n  &:after {\n    width: 120px;\n    text-align: ', ';\n    content:\'', '\';\n    display:', ';\n    position: absolute;\n    font-size: 12px;\n    color: black;\n    top: 0.5rem;\n    right: ', ';\n  }\n'], ['\n  position: absolute;\n  height: ', 'px;\n  width: ', 'px;\n  border-radius: ', 'px;\n  background-color: ', ';\n  left: ', ';\n  top: 0px;\n  bottom: 0px;\n  &:after {\n    width: 120px;\n    text-align: ', ';\n    content:\'', '\';\n    display:', ';\n    position: absolute;\n    font-size: 12px;\n    color: black;\n    top: 0.5rem;\n    right: ', ';\n  }\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }






var Progress = function Progress(_ref) {
  var labeled = _ref.labeled,
      width = _ref.width,
      progress = _ref.progress,
      aborted = _ref.aborted,
      states = _ref.states;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    ProgressBar,
    { labeled: labeled, width: width, color: aborted ? __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.danger : __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.well, __source: {
        fileName: _jsxFileName,
        lineNumber: 7
      }
    },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(ProgressBar, { width: width, progress: progress, color: __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.primary, __source: {
        fileName: _jsxFileName,
        lineNumber: 8
      }
    }),
    states.map(function (s, i) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Pt, {
        width: width,
        first: i == 0,
        last: i == states.length - 1,
        key: i,
        label: s,
        color: __WEBPACK_IMPORTED_MODULE_3__lib_theme__["a" /* default */].colors.black,
        labeled: labeled,
        position: i * (100 / (states.length - 1)),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 10
        }
      });
    })
  );
};

Progress.defaultProps = {
  width: 4
};

Progress.propTypes = {
  labeled: __WEBPACK_IMPORTED_MODULE_2_prop_types__["bool"],
  width: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
  progress: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
  aborted: __WEBPACK_IMPORTED_MODULE_2_prop_types__["bool"],
  states: Object(__WEBPACK_IMPORTED_MODULE_2_prop_types__["arrayOf"])(__WEBPACK_IMPORTED_MODULE_2_prop_types__["string"]).isRequired
};

var ProgressBar = __WEBPACK_IMPORTED_MODULE_1_styled_components___default.a.div(_templateObject, function (props) {
  return props.width;
}, function (props) {
  return !!props.progress ? props.progress * 100 + "%" : "100%";
}, function (props) {
  return props.width / 2;
}, function (props) {
  return props.color;
}, function (props) {
  return props.labeled ? "1rem" : "0";
});

ProgressBar.propTypes = {
  labeled: __WEBPACK_IMPORTED_MODULE_2_prop_types__["bool"],
  width: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
  color: __WEBPACK_IMPORTED_MODULE_2_prop_types__["string"].isRequired,
  progress: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"]
};

var Pt = __WEBPACK_IMPORTED_MODULE_1_styled_components___default.a.div(_templateObject2, function (props) {
  return props.width;
}, function (props) {
  return props.width;
}, function (props) {
  return props.width / 2;
}, function (props) {
  return props.color;
}, function (props) {
  return props.position == 100 ? 'calc(100% - ' + props.width + 'px)' : props.position + "%";
}, function (props) {
  if (props.first) return "left";
  if (props.last) return "right";
  return "center";
}, function (props) {
  return props.label;
}, function (props) {
  return props.labeled ? "initial" : "none";
}, function (props) {
  if (props.first) return '-' + (120 - props.width) + 'px';
  if (props.last) return '0';
  return '-' + (60 - props.width / 2) + 'px';
});

Pt.propTypes = {
  first: __WEBPACK_IMPORTED_MODULE_2_prop_types__["bool"],
  last: __WEBPACK_IMPORTED_MODULE_2_prop_types__["bool"],
  label: __WEBPACK_IMPORTED_MODULE_2_prop_types__["string"],
  labeled: __WEBPACK_IMPORTED_MODULE_2_prop_types__["bool"],
  width: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired,
  color: __WEBPACK_IMPORTED_MODULE_2_prop_types__["string"].isRequired,
  position: __WEBPACK_IMPORTED_MODULE_2_prop_types__["number"].isRequired
};



/***/ }),

/***/ "./components/rebass/switch.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Swtch; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
var _templateObject = _taggedTemplateLiteral(['\n  height: 18px;\n  width: 36px;\n  box-shadow: none;\n  background-color: ', ';\n  &:after {\n    height: 12px;\n    width: 12px;\n    margin: 3px;\n    background-color: ', ';\n  }\n'], ['\n  height: 18px;\n  width: 36px;\n  box-shadow: none;\n  background-color: ', ';\n  &:after {\n    height: 12px;\n    width: 12px;\n    margin: 3px;\n    background-color: ', ';\n  }\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }





var Swtch = __WEBPACK_IMPORTED_MODULE_0_styled_components___default()(__WEBPACK_IMPORTED_MODULE_1_rebass__["Switch"])(_templateObject, function (props) {
  return props.theme.colors.inset;
}, function (props) {
  return props.checked ? props.theme.colors.primary : props.theme.colors.white;
});

Swtch.propTypes = {
  checked: __WEBPACK_IMPORTED_MODULE_2_prop_types__["bool"]
};



/***/ }),

/***/ "./components/rebass/tabs.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Tb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Tbs; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
var _templateObject = _taggedTemplateLiteral(['\n  background-color: ', ';\n  border: none;\n  margin: 0.5rem 0;\n  border-radius: 3px;\n  & * {\n    margin: 0.2rem;\n  }\n'], ['\n  background-color: ', ';\n  border: none;\n  margin: 0.5rem 0;\n  border-radius: 3px;\n  & * {\n    margin: 0.2rem;\n  }\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  font-weight: ', ';\n  cursor: pointer;\n  font-size: 12px;\n  padding: 0.75rem 0.5rem;\n  margin:0;\n  border: none;\n  opacity: ', '\n  &:hover {\n    color: ', ';\n  }\n'], ['\n  font-weight: ', ';\n  cursor: pointer;\n  font-size: 12px;\n  padding: 0.75rem 0.5rem;\n  margin:0;\n  border: none;\n  opacity: ', '\n  &:hover {\n    color: ', ';\n  }\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }





var Tbs = __WEBPACK_IMPORTED_MODULE_0_styled_components___default()(__WEBPACK_IMPORTED_MODULE_1_rebass__["Tabs"])(_templateObject, function (props) {
  return props.theme.colors.inset;
});

var Tb = __WEBPACK_IMPORTED_MODULE_0_styled_components___default()(__WEBPACK_IMPORTED_MODULE_1_rebass__["Tab"])(_templateObject2, function (props) {
  return props.current ? 700 : 400;
}, function (props) {
  return props.current ? 1 : 0.5;
}, function (props) {
  return props.theme.colors.primary;
});

Tb.propTypes = {
  current: __WEBPACK_IMPORTED_MODULE_2_prop_types__["bool"]
};



/***/ }),

/***/ "./components/rebass/text.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Title; });
/* unused harmony export Body */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Caption; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Pre; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_styled_components__);
var _templateObject = _taggedTemplateLiteral(['\n  font-weight: 400;\n  font-size: 16px;\n  margin: 0;\n  color: ', ';\n'], ['\n  font-weight: 400;\n  font-size: 16px;\n  margin: 0;\n  color: ', ';\n']),
    _templateObject2 = _taggedTemplateLiteral(['\n  font-weight: 400;\n  font-size: 14px;\n  margin: 0;\n  color: ', ';\n'], ['\n  font-weight: 400;\n  font-size: 14px;\n  margin: 0;\n  color: ', ';\n']),
    _templateObject3 = _taggedTemplateLiteral(['\n  font-size: 12px;\n  margin: 0;\n  color: ', ';\n'], ['\n  font-size: 12px;\n  margin: 0;\n  color: ', ';\n']),
    _templateObject4 = _taggedTemplateLiteral(['\n  font-size: 10px;\n  font-family: "SF Mono", "Roboto Mono", Menlo, monospace;\n  color: ', ';\n'], ['\n  font-size: 10px;\n  font-family: "SF Mono", "Roboto Mono", Menlo, monospace;\n  color: ', ';\n']);

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }





var Title = __WEBPACK_IMPORTED_MODULE_2_styled_components___default.a.h3(_templateObject, function (props) {
  return props.theme.colors.black;
});

var Body = __WEBPACK_IMPORTED_MODULE_2_styled_components___default.a.p(_templateObject2, function (props) {
  return props.theme.colors.black;
});

var Caption = __WEBPACK_IMPORTED_MODULE_2_styled_components___default.a.p(_templateObject3, function (props) {
  return props.theme.colors.black;
});

var Pre = __WEBPACK_IMPORTED_MODULE_2_styled_components___default()(__WEBPACK_IMPORTED_MODULE_1_rebass__["Pre"])(_templateObject4, function (props) {
  return props.theme.colors.white;
});

/***/ }),

/***/ "./components/stats.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Stats; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WllStats; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__("prop-types");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__rebass__ = __webpack_require__("./components/rebass/index.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/components/stats.js';

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var Stat = function Stat(_ref) {
  var stat = _ref.stat;
  var label = stat.label,
      value = stat.value;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    __WEBPACK_IMPORTED_MODULE_4__rebass__["f" /* Caption */],
    { style: { textAlign: "center" }, __source: {
        fileName: _jsxFileName,
        lineNumber: 9
      }
    },
    label,
    ':',
    ' ',
    value
  );
};

var Stats = function (_Component) {
  _inherits(Stats, _Component);

  function Stats() {
    var _ref2;

    var _temp, _this, _ret;

    _classCallCheck(this, Stats);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = Stats.__proto__ || Object.getPrototypeOf(Stats)).call.apply(_ref2, [this].concat(args))), _this), _this.state = {}, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Stats, [{
    key: 'getInitialProps',
    value: function getInitialProps(_ref3) {
      _objectDestructuringEmpty(_ref3);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          data = _props.data,
          props = _objectWithoutProperties(_props, ['data']);

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_4__rebass__["d" /* BtnGroup */],
        _extends({}, props, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 23
          }
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_4__rebass__["e" /* BtnRow */],
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 24
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Stat, { stat: data[0], __source: {
              fileName: _jsxFileName,
              lineNumber: 25
            }
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Stat, { stat: data[1], __source: {
              fileName: _jsxFileName,
              lineNumber: 26
            }
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Stat, { stat: data[2], __source: {
              fileName: _jsxFileName,
              lineNumber: 27
            }
          })
        )
      );
    }
  }]);

  return Stats;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Stats.propTypes = {
  data: Object(__WEBPACK_IMPORTED_MODULE_1_prop_types__["arrayOf"])(Object(__WEBPACK_IMPORTED_MODULE_1_prop_types__["shape"])({
    label: __WEBPACK_IMPORTED_MODULE_1_prop_types__["string"].isRequired,
    value: __WEBPACK_IMPORTED_MODULE_1_prop_types__["node"].isRequired
  })).isRequired
};

var WllStats = function (_Component2) {
  _inherits(WllStats, _Component2);

  function WllStats() {
    var _ref4;

    var _temp2, _this2, _ret2;

    _classCallCheck(this, WllStats);

    for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    return _ret2 = (_temp2 = (_this2 = _possibleConstructorReturn(this, (_ref4 = WllStats.__proto__ || Object.getPrototypeOf(WllStats)).call.apply(_ref4, [this].concat(args))), _this2), _this2.state = {}, _temp2), _possibleConstructorReturn(_this2, _ret2);
  }

  _createClass(WllStats, [{
    key: 'getInitialProps',
    value: function getInitialProps(_ref5) {
      _objectDestructuringEmpty(_ref5);
    }
  }, {
    key: 'render',
    value: function render() {
      var data = this.props.data;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_4__rebass__["z" /* Wll */],
        { style: { marginBottom: 0, display: "block" }, __source: {
            fileName: _jsxFileName,
            lineNumber: 51
          }
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_4__rebass__["e" /* BtnRow */],
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 52
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Stat, { stat: data[0], __source: {
              fileName: _jsxFileName,
              lineNumber: 53
            }
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Stat, { stat: data[1], __source: {
              fileName: _jsxFileName,
              lineNumber: 54
            }
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Stat, { stat: data[2], __source: {
              fileName: _jsxFileName,
              lineNumber: 55
            }
          })
        )
      );
    }
  }]);

  return WllStats;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

WllStats.propTypes = {
  data: Object(__WEBPACK_IMPORTED_MODULE_1_prop_types__["arrayOf"])(Object(__WEBPACK_IMPORTED_MODULE_1_prop_types__["shape"])({
    label: __WEBPACK_IMPORTED_MODULE_1_prop_types__["string"].isRequired,
    value: __WEBPACK_IMPORTED_MODULE_1_prop_types__["node"].isRequired
  })).isRequired
};



/***/ }),

/***/ "./data/acceleration.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return test; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utility_random__ = __webpack_require__("./utility/random.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_new_array__ = __webpack_require__("new-array");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_new_array___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_new_array__);



var test = function test() {
  var limit = bounded(0, 100);
  return __WEBPACK_IMPORTED_MODULE_1_new_array___default()(300).reduce(function (a, _) {
    var prev = a[a.length - 1];
    var lower = limit(prev - Object(__WEBPACK_IMPORTED_MODULE_0__utility_random__["c" /* randomInt */])(1, 8));
    var upper = limit(prev + Object(__WEBPACK_IMPORTED_MODULE_0__utility_random__["c" /* randomInt */])(1, 8));
    a.push(Object(__WEBPACK_IMPORTED_MODULE_0__utility_random__["a" /* random */])(lower, upper));
    return a;
  }, [0]).map(function (e, i) {
    return {
      x: i,
      y: e
    };
  });
};

var bounded = function bounded(min, max) {
  return function (value) {
    if (value < min) return min;
    if (value > max) return max;
    return value;
  };
};

/***/ }),

/***/ "./data/batteries.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return test; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utility_random__ = __webpack_require__("./utility/random.js");


var test = function test() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utility_random__["b" /* randomArray */])(__WEBPACK_IMPORTED_MODULE_0__utility_random__["a" /* random */])(0, 12, 16).map(function (v) {
    return {
      voltage: v,
      status: randomStatus()
    };
  });
};

var randomStatus = function randomStatus() {
  var intIndex = Object(__WEBPACK_IMPORTED_MODULE_0__utility_random__["c" /* randomInt */])(0, 2);
  var statuses = ["active", "defective", "unactive"];
  return statuses[intIndex];
};

/***/ }),

/***/ "./data/states.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return states; });
var states = ["uninitiated", "preflight", "acceleration", "coast", "brake", "stopped", "service propulsion"];

/***/ }),

/***/ "./pages/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__("styled-components");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_next_router__ = __webpack_require__("next/router");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_next_router___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_next_router__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_lib_theme__ = __webpack_require__("./components/lib/theme.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass__ = __webpack_require__("rebass");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rebass___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rebass__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_rebass__ = __webpack_require__("./components/rebass/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_nav__ = __webpack_require__("./components/nav.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_card__ = __webpack_require__("./components/card/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__data_batteries__ = __webpack_require__("./data/batteries.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__data_acceleration__ = __webpack_require__("./data/acceleration.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__data_states__ = __webpack_require__("./data/states.js");
var _jsxFileName = '/Users/davidsun/Code/hyperloop-ui/pages/index.js';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _objectDestructuringEmpty(obj) { if (obj == null) throw new TypeError("Cannot destructure undefined"); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }


















var Comp = function (_Component) {
  _inherits(Comp, _Component);

  function Comp() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Comp);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Comp.__proto__ || Object.getPrototypeOf(Comp)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      percentage: 0,
      frame: 0,
      controls: false
    }, _this.nextFrame = function () {
      var _this$state = _this.state,
          percentage = _this$state.percentage,
          frame = _this$state.frame;

      var newPercentage = percentage;
      var newFrame = frame;
      if (percentage < 1) newPercentage += 0.001;
      if (newFrame < 300) newFrame++;
      _this.setState({ percentage: newPercentage, frame: newFrame });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Comp, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      //this.timer = setInterval(this.nextFrame, 1);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      //clearInterval(this.timer);
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          percentage = _state.percentage,
          frame = _state.frame,
          controls = _state.controls;
      var _props = this.props,
          batteries = _props.batteries,
          acceleration = _props.acceleration,
          velocity = _props.velocity;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_1_styled_components__["ThemeProvider"],
        { theme: __WEBPACK_IMPORTED_MODULE_3__components_lib_theme__["a" /* default */], __source: {
            fileName: _jsxFileName,
            lineNumber: 65
          }
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_5__components_rebass__["h" /* Container */],
          {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 66
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__components_nav__["a" /* default */], {
            checked: controls,
            onSwitchClick: function onSwitchClick() {
              __WEBPACK_IMPORTED_MODULE_2_next_router___default.a.push({
                pathname: '/controls'
              });
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 67
            }
          }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_5__components_rebass__["i" /* DeskContainer */],
            {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 75
              }
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_4_rebass__["Flex"],
              { mb: 2, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 76
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_5__components_rebass__["k" /* FlexColumn */],
                { pr: 1, width: 1 / 4, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 77
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["d" /* Network */], {
                  expand: true,
                  status: "Connected",
                  rate: 4,
                  record: {
                    number: 2,
                    date: "6/23/18",
                    location: "Test Center"
                  },
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 78
                  }
                })
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_5__components_rebass__["k" /* FlexColumn */],
                { pl: 1, width: 3 / 4, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 89
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["e" /* Progress */], {
                  expand: true,
                  aborted: false,
                  current: "Braking",
                  next: "Service Propulsion",
                  states: __WEBPACK_IMPORTED_MODULE_10__data_states__["a" /* states */],
                  totalProgress: 0.2,
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 90
                  }
                })
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_4_rebass__["Flex"],
              { mb: 2, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 100
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_5__components_rebass__["k" /* FlexColumn */],
                { pr: 1, width: 1 / 3, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 101
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["j" /* ThreeView */], {
                  stats: {
                    yaw: 2.2342,
                    pitch: 13.434,
                    roll: 432.12
                  },
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 102
                  }
                })
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_5__components_rebass__["k" /* FlexColumn */],
                { px: 1, width: 1 / 3, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 110
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["b" /* Chart */], {
                  title: 'Velocity',
                  meterData: { min: 0, max: 100.0, unit: "m/s" },
                  data: acceleration.slice(0, 300),
                  stats: { min: 0, max: 47.23, avg: 34.12 },
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 111
                  }
                })
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_5__components_rebass__["k" /* FlexColumn */],
                { pl: 1, width: 1 / 3, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 118
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["b" /* Chart */], {
                  title: 'Acceleration',
                  meterData: { min: 0, max: 100.0, unit: "m/s^2" },
                  data: acceleration.slice(0, 300),
                  stats: { min: 0, max: 47.23, avg: 34.12 },
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 119
                  }
                })
              )
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_4_rebass__["Flex"],
              { flexGrow: 1, __source: {
                  fileName: _jsxFileName,
                  lineNumber: 127
                }
              },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_5__components_rebass__["k" /* FlexColumn */],
                { pr: 1, width: 1 / 3, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 128
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["i" /* Table */], {
                  title: 'Table 1',
                  expand: true,
                  data: {
                    headers: ["head1", "head2", "head3", "head4"],
                    content: [{
                      "head1": "yo",
                      "head2": "2342",
                      "head3": "sdfsdf",
                      "head4": "5456"
                    }, {
                      "head1": "yo",
                      "head2": "2342",
                      "head3": "sdfsdf",
                      "head4": "5456"
                    }]
                  },
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 129
                  }
                })
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_5__components_rebass__["k" /* FlexColumn */],
                { px: 1, width: 1 / 3, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 154
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["i" /* Table */], {
                  title: 'Table 2',
                  expand: true,
                  data: {
                    headers: ["head1", "head2", "head3", "head4"],
                    content: [{
                      "head1": "yo",
                      "head2": "2342",
                      "head3": "sdfsdf",
                      "head4": "5456"
                    }, {
                      "head1": "yo",
                      "head2": "2342",
                      "head3": "sdfsdf",
                      "head4": "5456"
                    }]
                  },
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 155
                  }
                })
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                __WEBPACK_IMPORTED_MODULE_5__components_rebass__["k" /* FlexColumn */],
                { pl: 1, width: 1 / 3, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 180
                  }
                },
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["a" /* Battery */], {
                  mb: 2,
                  data: batteries,
                  stats: {
                    warning: 2,
                    status: "Stable",
                    batteries: 16
                  },
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 181
                  }
                }),
                __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_card__["g" /* Sponsor */], { expand: true, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 190
                  }
                })
              )
            )
          )
        )
      );
    }
  }], [{
    key: 'getInitialProps',
    value: function getInitialProps(_ref2) {
      _objectDestructuringEmpty(_ref2);

      return {
        batteries: __WEBPACK_IMPORTED_MODULE_8__data_batteries__["a" /* test */](),
        acceleration: __WEBPACK_IMPORTED_MODULE_9__data_acceleration__["a" /* test */](),
        velocity: __WEBPACK_IMPORTED_MODULE_9__data_acceleration__["a" /* test */]()
      };
    }
  }]);

  return Comp;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Comp);

/***/ }),

/***/ "./utility/random.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return randomInt; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return random; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return randomArray; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_new_array__ = __webpack_require__("new-array");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_new_array___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_new_array__);


var randomInt = function randomInt(min, max) {
  return Math.floor(random(min, max));
};

var random = function random(min, max) {
  return Math.random() * (max - min) + min;
};

var randomArray = function randomArray(func) {
  return function (min, max, ct) {
    return __WEBPACK_IMPORTED_MODULE_0_new_array___default()(ct).map(function () {
      return func(min, max);
    });
  };
};

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./pages/index.js");


/***/ }),

/***/ "grid-styled":
/***/ (function(module, exports) {

module.exports = require("grid-styled");

/***/ }),

/***/ "konva":
/***/ (function(module, exports) {

module.exports = require("konva");

/***/ }),

/***/ "new-array":
/***/ (function(module, exports) {

module.exports = require("new-array");

/***/ }),

/***/ "next/router":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "prop-types":
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "react":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-dom":
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "react-konva":
/***/ (function(module, exports) {

module.exports = require("react-konva");

/***/ }),

/***/ "react-vis":
/***/ (function(module, exports) {

module.exports = require("react-vis");

/***/ }),

/***/ "rebass":
/***/ (function(module, exports) {

module.exports = require("rebass");

/***/ }),

/***/ "styled-components":
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ }),

/***/ "three":
/***/ (function(module, exports) {

module.exports = require("three");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map