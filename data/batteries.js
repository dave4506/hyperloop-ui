import { randomArray, random, randomInt } from '../utility/random'

export const test = () => {
  return randomArray(random)(0,12,16).map((v)=>{
    return {
      voltage:v,
      status:randomStatus()
    }
  })
}

const randomStatus = () =>{
  const intIndex = randomInt(0,2);
  const statuses = ["active","defective","unactive"]
  return statuses[intIndex];
}
