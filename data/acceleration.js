import { randomArray, random, randomInt } from '../utility/random'
import newArray from 'new-array'

export const test = () => {
  const limit = bounded(0,100);
  return newArray(300).reduce((a,_)=>{
    const prev = a[a.length - 1];
    const lower = limit(prev - randomInt(1,8));
    const upper = limit(prev + randomInt(1,8));
    a.push(random(lower,upper));
    return a;
  },[0]).map((e,i)=>{
    return {
      x:i,
      y:e
    }
  })
}

const bounded = (min,max) => (value) => {
  if (value < min) return min;
  if (value > max) return max;
  return value;
}
