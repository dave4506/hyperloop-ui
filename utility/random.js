import newArray from 'new-array'

export const randomInt = (min,max) => {
  return Math.floor(random(min,max))
}

export const random = (min,max) => {
  return Math.random() * (max - min) + min;
}

export const randomArray = (func) => (min,max,ct) => {
  return newArray(ct).map(()=>{
    return func(min,max);
  })
}
