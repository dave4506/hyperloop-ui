import React, { Component } from "react"
import styled, { ThemeProvider } from 'styled-components'
import Router from 'next/router'
import theme from '../components/lib/theme'

import { Flex, Box } from 'rebass'

import { DeskContainer,
         Container,
         FlexColumn
       } from '../components/rebass'

import Nav from '../components/nav'

import { Progress,
         Network,
         Battery,
         Sponsor,
         Table,
         Chart,
         ThreeView
       } from '../components/card'

import * as batteryData from '../data/batteries'
import * as acclerationData from '../data/acceleration'
import * as statesData from '../data/states'

export default class Comp extends Component {
  state = {
    percentage: 0,
    frame: 0,
    controls: false
  }

  static getInitialProps({}) {
    return {
      batteries:batteryData.test(),
      acceleration:acclerationData.test(),
      velocity:acclerationData.test()
    }
  }

  nextFrame = () => {
    const { percentage, frame } = this.state;
    let newPercentage = percentage;
    let newFrame = frame;
    if (percentage < 1)
      newPercentage += 0.001
    if (newFrame < 300)
      newFrame++;
    this.setState({percentage:newPercentage,frame:newFrame})
  }

  componentDidMount() {
    //this.timer = setInterval(this.nextFrame, 1);
  }

  componentWillUnmount() {
    //clearInterval(this.timer);
  }

  render() {
    const { percentage, frame, controls } = this.state;
    const { batteries, acceleration, velocity } = this.props;
    return <ThemeProvider theme={theme}>
      <Container>
        <Nav
          checked={controls}
          onSwitchClick={()=>{
            Router.push({
              pathname: '/controls'
            })
          }}
          />
        <DeskContainer>
          <Flex mb={2}>
            <FlexColumn pr={1} width={1/4}>
              <Network
                expand
                status={"Connected"}
                rate={4}
                record={{
                  number: 2,
                  date:"6/23/18",
                  location:"Test Center"
                }}
              />
            </FlexColumn>
            <FlexColumn pl={1} width={3/4}>
              <Progress
                expand
                aborted={false}
                current={"Braking"}
                next={"Service Propulsion"}
                states={statesData.states}
                totalProgress={0.2}
              />
            </FlexColumn>
          </Flex>
          <Flex mb={2}>
            <FlexColumn pr={1} width={1/3}>
              <ThreeView
                stats={{
                  yaw:2.2342,
                  pitch:13.434,
                  roll:432.12
                }}
              />
            </FlexColumn>
            <FlexColumn px={1} width={1/3}>
              <Chart
                title="Velocity"
                meterData={{min:0,max:100.0,unit:"m/s"}}
                data={acceleration.slice(0,300)}
                stats={{min:0,max:47.23,avg:34.12}}
              />
            </FlexColumn>
            <FlexColumn pl={1} width={1/3}>
              <Chart
                title="Acceleration"
                meterData={{min:0,max:100.0,unit:"m/s^2"}}
                data={acceleration.slice(0,300)}
                stats={{min:0,max:47.23,avg:34.12}}
              />
            </FlexColumn>
          </Flex>
          <Flex flexGrow={1} >
            <FlexColumn pr={1} width={1/3}>
              <Table
                title="Table 1"
                expand
                data={{
                  headers:[
                    "head1",
                    "head2",
                    "head3",
                    "head4"
                  ],
                  content:[{
                    "head1":"yo",
                    "head2":"2342",
                    "head3":"sdfsdf",
                    "head4":"5456"
                  },
                  {
                    "head1":"yo",
                    "head2":"2342",
                    "head3":"sdfsdf",
                    "head4":"5456"
                  }]
                }}
              />
            </FlexColumn>
            <FlexColumn px={1} width={1/3}>
              <Table
                title="Table 2"
                expand
                data={{
                  headers:[
                    "head1",
                    "head2",
                    "head3",
                    "head4"
                  ],
                  content:[{
                    "head1":"yo",
                    "head2":"2342",
                    "head3":"sdfsdf",
                    "head4":"5456"
                  },
                  {
                    "head1":"yo",
                    "head2":"2342",
                    "head3":"sdfsdf",
                    "head4":"5456"
                  }]
                }}
              />
            </FlexColumn>
            <FlexColumn pl={1} width={1/3}>
              <Battery
                mb={2}
                data={batteries}
                stats={{
                  warning: 2,
                  status: "Stable",
                  batteries: 16
                }}
              />
              <Sponsor expand/>
            </FlexColumn>
          </Flex>
        </DeskContainer>
      </Container>
    </ThemeProvider>
  }
}
