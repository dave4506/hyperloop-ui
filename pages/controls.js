import React, { Component } from "react"
import styled, { ThemeProvider } from 'styled-components'
import Router from 'next/router'
import theme from '../components/lib/theme'

import { Flex, Box } from 'rebass'
import { DeskContainer,
         Container,
         FlexColumn
       } from '../components/rebass'

import Nav from '../components/nav'

import { SmallProgress,
         Network,
         Battery,
         Table,
         Chart,
         Stats,
         Controls
       } from '../components/card'

import * as batteryData from '../data/batteries'
import * as acclerationData from '../data/acceleration'
import * as statesData from '../data/states'

export default class Comp extends Component {
  state = {
    percentage: 0,
    frame: 0,
    controls: true
  }

  static getInitialProps({}) {
    return {
      batteries:batteryData.test(),
      acceleration:acclerationData.test(),
      velocity:acclerationData.test()
    }
  }

  nextFrame = () => {
    const { percentage, frame } = this.state;
    let newPercentage = percentage;
    let newFrame = frame;
    if (percentage < 1)
      newPercentage += 0.001
    if (newFrame < 300)
      newFrame++;
    this.setState({percentage:newPercentage,frame:newFrame})
  }

  componentDidMount() {
    //this.timer = setInterval(this.nextFrame, 1);
  }

  componentWillUnmount() {
    //clearInterval(this.timer);
  }

  render() {
    const { percentage, frame, controls } = this.state;
    const { batteries, acceleration, velocity } = this.props;
    return <ThemeProvider theme={theme}>
      <Container>
        <Nav
          checked={controls}
          onSwitchClick={()=>{
            Router.push({
              pathname: '/index'
            })
          }}
          />
        <DeskContainer>
          <Flex style={{height:"100%"}}>
            <FlexColumn pr={1} width={1/3}>
              <Box mb={2}>
                <Network
                  status={"Connected"}
                  rate={4}
                  record={{
                    number: 2,
                    date:"6/23/18",
                    location:"Test Center"
                  }}
                />
              </Box>
              <Stats
                data={{
                  yaw:2.2342,
                  pitch:13.434,
                  roll:432.12
                }}
                mb={2}
                title={"Orientation"}
              />
              <Battery
                mb={2}
                data={batteries}
                stats={{
                  warning: 2,
                  status: "Stable",
                  batteries: 16
                }}
              />
              <Table
                expand
                title="Table 2"
                data={{
                  headers:[
                    "head1",
                    "head2",
                    "head3",
                    "head4"
                  ],
                  content:[{
                    "head1":"yo",
                    "head2":"2342",
                    "head3":"sdfsdf",
                    "head4":"5456"
                  },
                  {
                    "head1":"yo",
                    "head2":"2342",
                    "head3":"sdfsdf",
                    "head4":"5456"
                  }]
                }}
              />
            </FlexColumn>
            <FlexColumn px={1} width={1/3}>
              <SmallProgress
                mb={2}
                aborted={false}
                current={"Braking"}
                next={"Service Propulsion"}
                states={statesData.states}
                totalProgress={0.2}
              />
              <Chart
                title="Velocity"
                data={velocity.slice(0,300)}
                meterData={{min:0,max:100.0,unit:"m/s"}}
                stats={{min:0,max:47.23,avg:34.12}}
                mb={2}
              />
              <Chart
                mb={2}
                title="Acceleration"
                meterData={{min:0,max:100.0,unit:"m/s^2"}}
                data={acceleration.slice(0,300)}
                stats={{min:0,max:47.23,avg:34.12}}
              />
              <Table
                title="Table 1"
                expand
                data={{
                  headers:[
                    "head1",
                    "head2",
                    "head3",
                    "head4"
                  ],
                  content:[{
                    "head1":"yo",
                    "head2":"2342",
                    "head3":"sdfsdf",
                    "head4":"5456"
                  },
                  {
                    "head1":"yo",
                    "head2":"2342",
                    "head3":"sdfsdf",
                    "head4":"5456"
                  }]
                }}
              />
            </FlexColumn>
            <FlexColumn pl={1} width={1/3}>
              <Controls/>
            </FlexColumn>
          </Flex>
        </DeskContainer>
      </Container>
    </ThemeProvider>
  }
}
