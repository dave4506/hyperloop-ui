import Document, { Head, Main, NextScript } from 'next/document'
import { ServerStyleSheet, injectGlobal, ThemeProvider } from 'styled-components'
import { Provider } from 'rebass'
injectGlobal`
  * { box-sizing: border-box; font-family: Helvetica; }
  body { margin: 0; }
`

export default class Doc extends Document {

  static getInitialProps ({ renderPage }) {
    const sheet = new ServerStyleSheet()
    const page = renderPage(App => props => sheet.collectStyles(<App {...props} />))
    const styleTags = sheet.getStyleElement()
    return { ...page, styleTags }
  }

  render () {
    return (
      <html>
        <Head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link rel="stylesheet" href="/static/vis.css" />
          {this.props.styleTags}
        </Head>
        <body>
          <Provider>
              <Main />
          </Provider>
          <NextScript />
        </body>
      </html>
    )
  }
}
